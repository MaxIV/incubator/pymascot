from time import time

from mascot.server import Device, attribute, command
from mascot.tango import AttrQuality
from mascot.database import Database
from mascot.dataclasses import DbDevInfo


class TestDevice(Device):
    ampli = attribute(dtype=int)
    bool_attr = attribute(dtype=bool)
    float_attr = attribute(dtype=float)
    string_attr = attribute(dtype=str)
    error = attribute(dtype=bool, fget="raise_error")

    @command
    def simple_cmd(self):
        pass

    def read_ampli(self):
        return 43

    def read_bool_attr(self):
        return True, AttrQuality.ATTR_INVALID

    def read_float_attr(self):
        return 34.5, 3550234237.123

    def read_string_attr(self):
        return ("Hello World", AttrQuality.ATTR_INVALID, time())

    def write_ampli(self, value):
        print("Write ampli {}".format(value))

    def raise_error(self):
        raise ValueError("Some Error")


if __name__ == "__main__":
    db = Database()
    info = DbDevInfo(klass="TestDevice", server="MascotTest/1", name="a/b/c")
    db.add_device(info)
    TestDevice.run_server()
