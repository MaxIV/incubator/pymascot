#!/usr/bin/env python

# Imports
import os

from setuptools import setup, find_packages

# Setup
setup(
    name="mascot",
    version="0.1.0",
    description="The mascot implementation in python",
    packages=find_packages(),
    author="Antoine Dupre",
    author_email="antoine.dupre@maxiv.lu.se",
    license="GPLv3",
    url="http://www.maxiv.lu.se",
    install_requires=["setuptools", "grpcio", "protobuf"],
)
