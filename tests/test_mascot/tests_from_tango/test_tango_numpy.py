from mascot import tango_numpy
from mascot.tango_enums import CmdArgType as ArgType
from mascot.attribute import Attribute
import pytest


arg_types = (
    ArgType.DevShort,
    ArgType.DevLong,
    ArgType.DevDouble,
    ArgType.DevFloat,
    ArgType.DevBoolean,
    ArgType.DevUShort,
    ArgType.DevULong,
    ArgType.DevUChar,
    ArgType.DevLong64,
)


@pytest.fixture(params=arg_types)
def tango_scalar_type(request):
    return request.param


@pytest.fixture(params=(int, float, bool, str))
def python_scalar_type(request):
    return request.param


def test_invalid_numpy():
    from mascot import constants

    constants.NUMPY_SUPPORT = False
    expected = (
        None,
        tango_numpy._numpy_invalid,
        tango_numpy._numpy_invalid,
        tango_numpy._numpy_invalid,
    )
    assert expected == tango_numpy._define_numpy()


def test_argtype_scalar(tango_scalar_type):
    assert tango_numpy.numpy_type(tango_scalar_type)


def test_python_scalar(python_scalar_type):
    attr = Attribute(dtype=python_scalar_type)
    if python_scalar_type == str:
        with pytest.raises(ValueError):
            tango_numpy.numpy_type(attr)
    else:
        assert tango_numpy.numpy_type(attr)
