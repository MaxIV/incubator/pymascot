# TODO: import from tango
import pytest

from mascot.server import Device
from mascot.tango_enums import DevState
from mascot.test_context import DeviceTestContext

# Test state/status

# ============== TMP ==============


@pytest.fixture(params=["GREEN MODE NOT IMPLEMENTED"])
def server_green_mode(request):
    return request.param


@pytest.fixture(params=["STATENOT IMPLEMENTED"])
def state(request):
    request.param


# ============================================


def test_empty_device(server_green_mode):
    pytest.xfail()

    class TestDevice(Device):
        green_mode = server_green_mode

    with DeviceTestContext(TestDevice) as proxy:
        assert proxy.state() == DevState.UNKNOWN
        assert proxy.status() == "The device is in UNKNOWN state."


def test_set_state(state, server_green_mode):
    pytest.xfail()
    status = "The device is in {0!s} state.".format(state)

    class TestDevice(Device):
        green_mode = server_green_mode

        def init_device(self):
            self.set_state(state)

    with DeviceTestContext(TestDevice) as proxy:
        assert proxy.state() == state
        assert proxy.status() == status


def test_set_status(server_green_mode):
    pytest.xfail()

    status = "\n".join(
        (
            "This is a multiline status",
            "with special characters such as",
            "Café à la crème",
        )
    )

    class TestDevice(Device):
        green_mode = server_green_mode

        def init_device(self):
            self.set_state(DevState.ON)
            self.set_status(status)

    with DeviceTestContext(TestDevice) as proxy:
        assert proxy.state() == DevState.ON
    assert proxy.status() == status
