import datetime
import time
import os

import pytest
from hypothesis import given
from hypothesis import strategies as st

from mascot.time_val import TimeVal

# Set timezone !
os.environ["TZ"] = "Europe/Stockholm"


def assert_timeval(tval, sec, usec, nsec):
    assert tval.tv_sec == sec
    assert tval.tv_usec == usec
    assert tval.tv_nsec == nsec
    # Test all the method
    assert isinstance(tval.totime(), float)
    assert isinstance(tval.todatetime(), datetime.datetime)
    assert len(tval.strftime("%T").split(":"))
    assert str(tval)


def test_timeval_init():
    date_t0 = datetime.datetime(1970, 1, 1, 1, 0, 0)
    # Empty
    assert_timeval(TimeVal(), 0, 0, 0)
    assert_timeval(TimeVal(a=1), 1, 0, 0)
    assert_timeval(TimeVal(b=1), 0, 0, 0)
    assert_timeval(TimeVal(c=1), 0, 0, 0)
    assert_timeval(TimeVal(a=0, b=1), 0, 1, 0)
    assert_timeval(TimeVal(a=0, c=1), 0, 0, 1)
    assert_timeval(TimeVal(a=1.100000011), 1, 100000, 11)
    expected_date = (time.mktime(date_t0.timetuple()), 0, 0)
    assert_timeval(TimeVal(date_t0), *expected_date)
    with pytest.raises(ValueError):
        TimeVal(date_t0, 1, 0)
    assert_timeval(TimeVal.fromdatetime(date_t0), *expected_date)
    assert_timeval(TimeVal.fromtimestamp(0), 0, 0, 0)
    assert TimeVal.now().tv_sec > 0
    tval = TimeVal(date_t0)
    assert tval.isoformat() == "1970-01-01T01:00:00"
    assert tval.strftime("%m/%d/%Y, %H:%M:%S") == "01/01/1970, 01:00:00"
    printout = "TimeVal(tv_sec = 0, tv_usec = 0, tv_nsec = 0)"
    assert repr(tval) == printout


@given(date_value=st.datetimes())
def test_timeval_init_datetime(date_value):
    nsec = 0
    usec = date_value.microsecond
    sec = int(date_value.strftime("%s"))
    tval = TimeVal(date_value)
    assert_timeval(tval, sec, usec, nsec)
