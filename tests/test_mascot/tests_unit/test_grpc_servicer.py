from mascot.grpc_servicer import DeviceServicer
from mascot.grpcproto.attributes_pb2 import DeviceAttribute
from mascot.grpcproto.attributes_pb2 import ReadAttributeMessage

from mascot.grpcproto.attribute_config_pb2 import AttributeConfig
from mock import MagicMock
import pytest


def test_read_attribute():
    test_device = MagicMock()
    name = "a/b/c"
    attr = "ampli"
    attr_message = ReadAttributeMessage(deviceName=name, attributeName=attr)
    device_attr = DeviceAttribute()
    service = DeviceServicer("SomeName")
    service.add_device(name, test_device)
    assert name in service.device_list
    test_device.read_attribute.return_value = device_attr
    rep = service.ReadAttribute(attr_message, None)
    assert rep.data == device_attr
    assert rep.success
    test_device.read_attribute.side_effect = Exception("ZBLA")
    rep = service.ReadAttribute(attr_message, None)
    assert not rep.success


def test_read_attribute_config():
    test_device = MagicMock()
    name = "a/b/c"
    attr = "ampli"
    attr_message = ReadAttributeMessage(deviceName=name, attributeName=attr)
    attr_cfg = AttributeConfig()
    service = DeviceServicer("SomeName")
    service.add_device(name, test_device)
    assert name in service.device_list
    test_device.read_attribute_config.return_value = attr_cfg
    rep = service.ReadAttributeConfig(attr_message, None)
    assert rep.data == attr_cfg
    assert rep.success
    test_device.read_attribute_config.side_effect = Exception("ZBLA")
    rep = service.ReadAttributeConfig(attr_message, None)
    assert not rep.success


def test_write_attribute():
    pytest.xfail()


def test_execupte_command():
    pytest.xfail()
