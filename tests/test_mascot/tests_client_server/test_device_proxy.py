from time import time

from mascot.server import Device, attribute
from mascot.tango import AttrQuality

from mascot.test_context import DeviceTestContext


class MascotDevice(Device):
    ampli = attribute(dtype=int)
    bool_attr = attribute(dtype=bool)
    float_attr = attribute(dtype=float)
    string_attr = attribute(dtype=str)
    error = attribute(dtype=bool, fget="raise_error")

    def read_ampli(self):
        return 43

    def read_bool_attr(self):
        return True, AttrQuality.ATTR_INVALID

    def read_float_attr(self):
        return 34.5, 3550234237.123

    def read_string_attr(self):
        return ("Hello World", AttrQuality.ATTR_INVALID, time())

    def write_ampli(self, value):
        print("Write ampli {}".format(value))

    def raise_error(self):
        raise ValueError("Some Error")


def test_deviceproxy_dir():
    attr_names = ["ampli", "bool_attr", "float_attr", "string_attr", "error"]
    with DeviceTestContext(MascotDevice, port=12345) as ds:
        ds_dir = dir(ds)
    for attr in attr_names:
        assert attr in ds_dir
