from time import time

import pytest

from mascot.errors import DevFailed
from mascot.server import Device, attribute
from mascot.tango import AttrQuality
from mascot.test_context import DeviceTestContext
from mascot import tango_numpy

# PyTango utils

TYPED_VALUES = {
    int: (1, 2, -65535, 23),
    float: (2.71, 3.14, -34.678e-10, 12.678e15),
    str: ("hey hey", "my my", "1234", "toto"),
    bool: (False, True, True, False),
}


def is_nan(num):
    return num != num


def repr_type(x):
    if not isinstance(x, tuple):
        return x.__name__
    return "({},)".format(x[0].__name__)


@pytest.fixture(params=list(TYPED_VALUES.items()), ids=lambda x: repr_type(x[0]))
def typed_values(request):
    dtype, values = request.param
    return dtype, values


def test_read_only_attribute(typed_values):
    set_timestamp = 1234
    now = time()
    dtype, values = typed_values
    attr_list_expected = [
        "ro_simple",
        "ro_quality",
        "ro_timestamp",
        "ro_all",
        "ro_error",
        "state",
        "status",
    ]

    class TestDevice(Device):
        ro_simple = attribute(dtype=dtype, fget="simple")
        ro_quality = attribute(dtype=dtype)
        ro_timestamp = attribute(dtype=dtype)
        ro_all = attribute(dtype=dtype)
        ro_error = attribute(dtype=dtype)

        def simple(self):
            return values[0]

        def read_ro_quality(self):
            return values[1], AttrQuality.ATTR_INVALID

        def read_ro_timestamp(self):
            return values[2], set_timestamp

        def read_ro_all(self):
            return values[3], AttrQuality.ATTR_INVALID, set_timestamp

        def read_ro_error(self):
            raise ValueError("Error")

    with DeviceTestContext(TestDevice, port=12345) as ds:
        attr_list = ds.get_attribute_list()
        assert attr_list == attr_list_expected
        ro_simple = ds.read_attribute("ro_simple")
        ro_quality = ds.read_attribute("ro_quality")
        ro_timestamp = ds.read_attribute("ro_timestamp")
        ro_all = ds.read_attribute("ro_all")
        # TODO use DevFailed
        with pytest.raises(Exception) as exc:
            ds.read_attribute("ro_error")
        assert ro_simple.value == values[0]
        # TODO time stamp > now
        assert ro_quality.value == values[1]
        # TODO: assert ro_quality.quality == AttrQuality.ATTR_INVALID
        assert ro_timestamp.value == values[2]
        # TODO timestamp == set_timestamp
        assert ro_all.value == values[3]
        assert exc.type == DevFailed


def test_read_write_attribute(typed_values):
    dtype, values = typed_values

    class TestDevice(Device):
        default = 0
        ampli = attribute(dtype=dtype)
        attr_error = attribute(dtype=dtype, fset="raise_error")

        def read_ampli(self):
            return self.default

        def write_ampli(self, value):
            self.default = value

        def raise_error(self, value):
            raise ValueError(f"Test with {value}")

    with DeviceTestContext(TestDevice, port=12345) as ds:
        for v in values:
            ds.write_attribute("ampli", v)
            assert ds.read_attribute("ampli").value == v
        # Test direct access
        for v in values:
            ds.ampli = v
            assert ds.ampli == v
            with pytest.raises(DevFailed):
                ds.write_attribute("attr_error", v)


def test_read_attribute_config(typed_values):
    dtype, _ = typed_values

    class TestDevice(Device):
        default = 0
        ampli = attribute(dtype=dtype)

        def read_ampli(self):
            return self.default

        def write_ampli(self, value):
            self.default = value

    with DeviceTestContext(TestDevice, port=12345) as ds:
        cfg = ds.get_attribute_config("ampli")
        # assert cfg.data_type == dtype
