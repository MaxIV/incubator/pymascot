from time import time

from mascot.server import Device, command
from mascot.tango import AttrQuality

from mascot.test_context import DeviceTestContext


def test_command_no_arg_no_return():
    class TestDeviceCommand(Device):
        has_run = False

        @command
        def MyCommand(self):
            self.has_run = True
            pass

    with DeviceTestContext(TestDeviceCommand, port=12345) as ds:
        print(ds.command_inout("MyCommand"))
        ds_server = ds._mascot_test_context_server.get_device_object("a/b/c")
        assert ds_server.has_run
