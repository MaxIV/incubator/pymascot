import pytest
from hypothesis import given
from hypothesis import strategies as st

from mascot.server import Device, attribute
from mascot.tango_enums import GreenMode
from mascot.test_context import DeviceTestContext
from mascot.utils import value_unpacker


def is_nan(num):
    return num != num


@pytest.fixture(params=[GreenMode.Synchronous, GreenMode.Asyncio])
def green_mode(request):
    return request.param


@given(
    int_value=st.integers(min_value=-9223372036854775808, max_value=9223372036854775807)
)
def test_read_write_int_attribute_hypothesis(int_value, green_mode):
    class TestDevice(Device):
        simple = attribute(dtype=int, fget="rsimple", fset="wsimple")

        def rsimple(self):
            return self._simple

        def wsimple(self, value):
            self._simple = value

    with DeviceTestContext(TestDevice, green_mode=green_mode, port=12345) as ds:
        ds.write_attribute("simple", int_value, wait=True)
        simple = ds.read_attribute("simple", wait=True)
        assert simple.value == int_value


@given(float_value=st.floats())
def test_read_write_float_attribute_hypothesis(float_value, green_mode):
    class TestDevice(Device):
        simple = attribute(dtype=float, fget="rsimple", fset="wsimple")

        def rsimple(self):
            return self._simple

        def wsimple(self, value):
            self._simple = value

    with DeviceTestContext(TestDevice, green_mode=green_mode, port=12345) as ds:
        ds.write_attribute("simple", float_value, wait=True)
        simple = ds.read_attribute("simple", wait=True)
        if is_nan(float_value):
            assert is_nan(simple.value)
        else:
            assert simple.value == float_value


@given(bool_value=st.booleans())
def test_read_write_bool_attribute_hypothesis(bool_value, green_mode):
    class TestDevice(Device):
        simple = attribute(dtype=bool, fget="rsimple", fset="wsimple")

        def rsimple(self):
            return self._simple

        def wsimple(self, value):
            self._simple = value

    with DeviceTestContext(TestDevice, green_mode=green_mode, port=12345) as ds:
        ds.write_attribute("simple", bool_value, wait=True)
        simple = ds.read_attribute("simple", wait=True)
        assert simple.value == bool_value


# TODO add binary ?
@given(str_value=st.text())
def test_read_write_str_attribute_hypothesis(str_value, green_mode):
    class TestDevice(Device):
        simple = attribute(dtype=str, fget="rsimple", fset="wsimple")

        def rsimple(self):
            return self._simple

        def wsimple(self, value):
            self._simple = value

    with DeviceTestContext(TestDevice, green_mode=green_mode, port=12345) as ds:
        ds.write_attribute("simple", str_value, wait=True)
        simple = ds.read_attribute("simple", wait=True)
        assert simple.value == str_value


#
# @given(st.binary(min_size=1))
# def test_read_write_bin_attribute_hypothesis(str_value):
#     print(str_value)
#     print("----")
#     class TestDevice(Device):
#         simple = attribute(dtype=str, fget="rsimple", fset="wsimple")
#
#         def rsimple(self):
#             return self._simple
#
#         def wsimple(self, value):
#             self._simple = value
#
#     with DeviceTestContext(TestDevice, port=12345) as ds:
#         ds.write_attribute("simple", str_value)
#         simple = ds.read_attribute("simple")
#         assert simple.value)[1] == str_value
