from mascot_registry.test_context import RegistryTestContext
import pytest
from hypothesis import given
import hypothesis.strategies as st
from hypothesis.strategies import characters
from mascot_registry.jsonregistry_server import Registry
import ipaddress
from contextlib import contextmanager


@pytest.fixture
def registry():
    with RegistryTestContext() as reg:
        yield reg


@contextmanager
def assert_create_device(client_registry, server_registry, device, klass, server):
    rep = client_registry.CreateDevice(device, klass, server)
    assert rep.success
    l_server = server.lower()
    assert l_server in server_registry.registry
    assert device in server_registry.registry[l_server]
    assert klass == server_registry.registry[l_server][device].mascotclass
    yield
    # Flush data
    del server_registry.registry
    server_registry.registry = Registry()


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
)
def test_create_device_hypothesis(dom, fam, mem, klass, server, registry):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    with assert_create_device(client_registry, server_registry, device, klass, server):
        pass


def test_delete_device(registry):
    server_registry, client_registry = registry
    device = "a/b/c"
    server = "Server/Test"
    l_server = server.lower()
    klass = "Class"

    with assert_create_device(client_registry, server_registry, device, klass, server):
        rep = client_registry.DeleteDevice(device)
        assert rep.success
        assert l_server in server_registry.registry
        assert device not in server_registry.registry[l_server]

        rep = client_registry.DeleteDevice(device)
        assert not rep.success
        assert l_server in server_registry.registry


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
)
def test_delete_device_hypothesis(dom, fam, mem, klass, server, registry):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        rep = client_registry.DeleteDevice(device)
        assert rep.success
        assert l_server in server_registry.registry
        assert device not in server_registry.registry[l_server]


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
    host=st.builds(
        ipaddress.IPv4Address, st.integers(min_value=0, max_value=(2 ** 32 - 1))
    ).map(str),
    port=st.integers(min_value=0, max_value=65535),
)
def test_publish_server_hypothesis(dom, fam, mem, klass, server, host, port, registry):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        assert server_registry.registry[l_server].host == ""
        assert server_registry.registry[l_server].port == 0

        rep = client_registry.PublishServer(server, host, port)
        assert rep.success
        assert l_server in server_registry.registry
        assert server_registry.registry[l_server].host == host
        assert server_registry.registry[l_server].port == port


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
    host=st.builds(
        ipaddress.IPv4Address, st.integers(min_value=0, max_value=(2 ** 32 - 1))
    ).map(str),
    port=st.integers(min_value=0, max_value=65535),
)
def test_unpublish_server_hypothesis(
    dom, fam, mem, klass, server, host, port, registry
):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        assert server_registry.registry[l_server].host == ""
        assert server_registry.registry[l_server].port == 0

        rep = client_registry.PublishServer(server, host, port)
        assert rep.success
        assert l_server in server_registry.registry
        assert server_registry.registry[l_server].host == host
        assert server_registry.registry[l_server].port == port

        rep = client_registry.UnpublishServer(server)
        assert rep.success
        assert l_server in server_registry.registry
        assert server_registry.registry[l_server].host == ""
        assert server_registry.registry[l_server].port == 0


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
)
def test_list_servers_hypothesis(dom, fam, mem, klass, server, registry):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        rep = client_registry.ListServers()
        assert rep.error.success
        assert len(rep.server_name) == 1


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
)
def test_list_devices_hypothesis(dom, fam, mem, klass, server, registry):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        rep = client_registry.ListServers()
        assert rep.error.success
        assert len(rep.server_name) == 1

        rep = client_registry.ListDevices(server)
        assert rep.error.success
        assert len(rep.devices) == 1
        assert rep.devices[0].name.family == fam
        assert rep.devices[0].name.domain == dom
        assert rep.devices[0].name.identifier == mem


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
    host=st.builds(
        ipaddress.IPv4Address, st.integers(min_value=0, max_value=(2 ** 32 - 1))
    ).map(str),
    port=st.integers(min_value=0, max_value=65535),
)
def test_publish_device_hypothesis(dom, fam, mem, klass, server, host, port, registry):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        assert not server_registry.registry[l_server][device].exported

        rep = client_registry.PublishDevice(device)
        assert rep.success
        assert l_server in server_registry.registry
        assert device in server_registry.registry[l_server]
        assert klass == server_registry.registry[l_server][device].mascotclass
        assert server_registry.registry[l_server][device].exported == True


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
    host=st.builds(
        ipaddress.IPv4Address, st.integers(min_value=0, max_value=(2 ** 32 - 1))
    ).map(str),
    port=st.integers(min_value=0, max_value=65535),
)
def test_unpublish_device_hypothesis(
    dom, fam, mem, klass, server, host, port, registry
):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        assert not server_registry.registry[l_server][device].exported

        rep = client_registry.PublishDevice(device)
        assert rep.success
        assert l_server in server_registry.registry
        assert device in server_registry.registry[l_server]
        assert klass == server_registry.registry[l_server][device].mascotclass
        assert server_registry.registry[l_server][device].exported

        rep = client_registry.UnpublishDevice(device)
        assert rep.success
        assert l_server in server_registry.registry
        assert device in server_registry.registry[l_server]
        assert klass == server_registry.registry[l_server][device].mascotclass
        assert not server_registry.registry[l_server][device].exported


@given(
    dom=st.text(min_size=1),
    fam=st.text(min_size=1),
    mem=st.text(min_size=1),
    server=st.text(min_size=1),
    klass=st.text(min_size=1),
    host=st.builds(
        ipaddress.IPv4Address, st.integers(min_value=0, max_value=(2 ** 32 - 1))
    ).map(str),
    port=st.integers(min_value=0, max_value=65535),
)
def test_locate_hypothesis(dom, fam, mem, klass, server, host, port, registry):
    if "/" in dom or "/" in fam or "/" in mem:
        return
    server_registry, client_registry = registry
    device = "/".join((dom, fam, mem))

    l_server = server.lower()
    with assert_create_device(client_registry, server_registry, device, klass, server):
        assert server_registry.registry[l_server].host == ""
        assert server_registry.registry[l_server].port == 0

        rep = client_registry.PublishServer(server, host, port)
        assert rep.success
        assert l_server in server_registry.registry
        assert server_registry.registry[l_server].host == host
        assert server_registry.registry[l_server].port == port

        rep = client_registry.Locate(device)
        assert rep.error.success
        assert rep.server_name == l_server
        assert rep.host == host
        assert rep.port == port
