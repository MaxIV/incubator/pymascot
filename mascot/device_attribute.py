from dataclasses import dataclass, field

from typing import Any
from mascot.tango_enums import AttrDataFormat, AttrQuality, CmdArgType
from mascot.utils import value_unpacker
from mascot.time_val import TimeVal


@dataclass
class AttributeDimension:
    dim_x: int = 0
    dim_y: int = 0


@dataclass
class DeviceAttribute:
    data_format: AttrDataFormat = AttrDataFormat.FMT_UNKNOWN
    dim_x: int = 0
    dim_y: int = 0
    has_failed: bool = False
    is_empty: bool = False
    name: str = "Name not set"
    nb_read: int = 0
    nb_written: int = 0
    quality: AttrQuality = AttrQuality.ATTR_INVALID
    r_dimension: AttributeDimension = field(default_factory=AttributeDimension)
    w_dimension: AttributeDimension = field(default_factory=AttributeDimension)
    time: TimeVal = field(default_factory=TimeVal)
    value: Any = None
    w_dim_x: int = 0
    w_dim_y: int = 0
    w_value: Any = None
    type: CmdArgType = None

    # TODO add ExctactAs get_date get_err_stack

    @staticmethod
    def build_from_grpc(grpc_dev_attr):
        # Name
        name = grpc_dev_attr.name
        # dtype
        data_format = AttrDataFormat(grpc_dev_attr.data_format)
        dtype = CmdArgType(grpc_dev_attr.type)
        # Read Attribute
        value = value_unpacker(grpc_dev_attr.value)[1]
        quality = AttrQuality(grpc_dev_attr.quality)
        time_value = TimeVal(
            grpc_dev_attr.time.tv_sec,
            grpc_dev_attr.time.tv_usec,
            grpc_dev_attr.time.tv_nsec,
        )
        r_dimension = AttributeDimension(
            grpc_dev_attr.r_dimension.dim_x, grpc_dev_attr.r_dimension.dim_x
        )
        dim_x = grpc_dev_attr.dim_x
        dim_y = grpc_dev_attr.dim_y
        # Write Attribute
        # TODO unpack it
        w_value = grpc_dev_attr.w_value
        w_dimension = AttributeDimension(
            grpc_dev_attr.w_dimension.dim_x, grpc_dev_attr.r_dimension.dim_x
        )
        w_dim_x = grpc_dev_attr.w_dim_x
        w_dim_y = grpc_dev_attr.w_dim_y
        # Extra
        has_failed = grpc_dev_attr.has_failed
        is_empty = grpc_dev_attr.is_empty
        nb_read = grpc_dev_attr.nb_read
        nb_written = grpc_dev_attr.nb_written
        # Create new object
        return DeviceAttribute(
            data_format=data_format,
            dim_x=dim_x,
            dim_y=dim_y,
            has_failed=has_failed,
            is_empty=is_empty,
            name=name,
            nb_read=nb_read,
            nb_written=nb_written,
            quality=quality,
            r_dimension=r_dimension,
            w_dimension=w_dimension,
            time=time_value,
            value=value,
            w_dim_x=w_dim_x,
            w_dim_y=w_dim_y,
            w_value=w_value,
            type=dtype,
        )
