import logging
from time import time

from mascot.attribute import attribute
from mascot.runners import run
from mascot.server_meta import DeviceMeta
from mascot.tango_enums import AttrQuality, CmdArgType

# Grpc and run server
logging.basicConfig(level=logging.DEBUG)


class Device(metaclass=DeviceMeta):
    """ Device class that can be insanciate by a mascot server """

    # Common attributes
    # state = attribute(dtype=CmdArgType.DevState, fget="dev_state")
    state = attribute(dtype=str, fget="dev_state")
    status = attribute(dtype=str, fget="dev_status")

    def __init__(self, name):
        self.__name = name
        # Setup logger
        self.logger = logging.getLogger(name)
        self.logger.info("Start device {}".format(name))
        # TODO, use DevState
        self.state.value = "UNKNOWN"
        self.status.value = "UNKNOWN"
        self.init_device()

    def init_device(self):
        pass

    def delete_device(self):
        pass

    def debug_stream(self, msg):
        self.logger.debug(msg)

    def info_stream(self, msg):
        self.logger.info(msg)

    def warn_stream(self, msg):
        self.logger.warn(msg)

    def error_stream(self, msg):
        self.logger.error(msg)

    def set_state(self, state):
        self.state.value = state

    def set_status(self, status):
        self.status.value = status
        self.__status = status

    def get_state(self):
        return self.state.value

    def get_status(self):
        return self.status.value

    def dev_status(self):
        return self.status.value

    def dev_state(self):
        return self.state.value

    def read_attr_hardware(self, attr_list):
        pass

    def always_executed_hook(self):
        pass

    def read_attribute(self, name):
        """ Read one attribute base on the attribute name"""
        # Get the mascot attribute
        attr = getattr(self, name)
        # Get the attribute read function
        attr_func = getattr(self, attr._fget)
        # Perform the read function
        values = attr_func()
        # Setup default
        value = None
        quality = AttrQuality.ATTR_VALID
        timestamp = time()
        # TODO What if value is a tupple len 2 or 3
        # Inspect the output of the read function
        if isinstance(values, tuple):
            # Only a value has been return
            if len(values) == 1:
                value = values[0]
            # 2 values, the first one is the read value, the second one
            # can be the quality or the timestamp
            elif len(values) == 2:
                value, other = values
                if isinstance(other, AttrQuality):
                    quality = other
                else:
                    timestamp = other
            # read function has return value quality and timestamp
            elif len(values) == 3:
                value, quality, timestamp = values
        else:
            # No tuple return, it is a single readvalue
            value = values
        # Update the attribute
        attr._value = value
        attr._quality = quality
        attr.set_timestamp(timestamp)
        # Build a reply
        return attr.build_attribute_reply()

    def read_attribute_config(self, name):
        attr = getattr(self, name)
        return attr.build_attribute_config_reply()

    def write_attribute(self, name, value):
        attr = getattr(self, name)
        if not attr._fset and hasattr(self, "write_{}".format(name)):
            attr._fset = "write_{}".format(name)
            attr_func = getattr(self, "write_{}".format(name))
        else:
            attr_func = getattr(self, attr._fset)
        attr.w_value = value
        attr_func(value)

    def exec_command(self, name, kwargs):
        # TODO define command generator
        command = getattr(self, name)
        print(command)
        # Todo, bound call to self
        return command.func_call(self, **kwargs)

    @classmethod
    def run_server(cls):
        run(cls)
