# Imported from Pytango: tango.time_val.py

import time
import datetime
import numbers


class TimeVal:
    tv_sec = 0
    tv_usec = 0
    tv_nsec = 0

    def __init__(self, a=None, b=None, c=None):
        if a is None:
            return

        if isinstance(a, datetime.datetime):
            if not (b is None and c is None):
                raise ValueError("With datetime, arg2 and arg3 as to be None")
            self.tv_sec = time.mktime(a.timetuple())
            print(a.timetuple())
            self.tv_usec = a.microsecond

        elif isinstance(a, numbers.Number):
            if b is None and c is None:
                self.tv_sec = int(a)
                usec = (a - self.tv_sec) * 1e6
                self.tv_usec = int(usec)
                self.tv_nsec = int((usec - self.tv_usec) * 1e3)
            else:
                self.tv_sec = a
                self.tv_usec = b if b else 0
                self.tv_nsec = c if c else 0

    def __repr__(self):
        nsec = f"tv_nsec = {int(self.tv_nsec)}"
        usec = f"tv_usec = {int(self.tv_usec)}"
        sec = f"tv_sec = {int(self.tv_sec)}"
        return f"TimeVal({sec}, {usec}, {nsec})"

    def __str__(self):
        return str(self.todatetime())

    def totime(self):
        return self.tv_sec + 1e-6 * self.tv_usec + 1e-9 * self.tv_nsec

    def todatetime(self):
        return datetime.datetime.fromtimestamp(self.totime())

    @staticmethod
    def fromtimestamp(ts):
        return TimeVal(ts)

    @staticmethod
    def fromdatetime(dt):
        return TimeVal(dt)

    @staticmethod
    def now():
        return TimeVal(time.time())

    def strftime(self, format):
        return self.todatetime().strftime(format)

    def isoformat(self, sep="T"):
        return self.todatetime().isoformat(sep)
