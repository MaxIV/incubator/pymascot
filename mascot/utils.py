from math import modf
import types
import psutil

import numbers
import collections.abc as collections_abc


from mascot import constants
from mascot.grpcproto.value_pb2 import AttributeValue
from mascot.tango_enums import CmdArgType

from socket import AddressFamily


def get_network_interfaces():
    addrs = []
    # Get all interfaces
    for snicaddrs in psutil.net_if_addrs().values():
        # Get all addresses for this interface
        for snicaddr in snicaddrs:
            # Filter on the AF_INET types
            if snicaddr.family == AddressFamily.AF_INET:
                snicaddr.address
                addrs.append(snicaddr.address)
    return addrs


def get_digits(number, start, stop):
    # Shift all digit before start before "."
    number = number * 10 ** start
    # Remove decimal part
    number = modf(number)[0]
    # Shift untile stop
    number = number * 10 ** (stop - start)
    return int(number)


def value_unpacker(value: AttributeValue):
    desc, val = value.ListFields()[0]
    return desc.name, val


def __build_to_tango_type():
    ret = {
        int: CmdArgType.DevLong64,
        str: CmdArgType.DevString,
        bool: CmdArgType.DevBoolean,
        bytearray: CmdArgType.DevEncoded,
        float: CmdArgType.DevDouble,
        chr: CmdArgType.DevUChar,
        None: CmdArgType.DevVoid,
        "int": CmdArgType.DevLong64,
        "int16": CmdArgType.DevShort,
        "int32": CmdArgType.DevLong,
        "int64": CmdArgType.DevLong64,
        "uint": CmdArgType.DevULong64,
        "uint16": CmdArgType.DevUShort,
        "uint32": CmdArgType.DevULong,
        "uint64": CmdArgType.DevULong64,
        "str": CmdArgType.DevString,
        "string": CmdArgType.DevString,
        "text": CmdArgType.DevString,
        "bool": CmdArgType.DevBoolean,
        "boolean": CmdArgType.DevBoolean,
        "bytes": CmdArgType.DevEncoded,
        "bytearray": CmdArgType.DevEncoded,
        "float": CmdArgType.DevDouble,
        "float32": CmdArgType.DevFloat,
        "float64": CmdArgType.DevDouble,
        "double": CmdArgType.DevDouble,
        "byte": CmdArgType.DevUChar,
        "chr": CmdArgType.DevUChar,
        "char": CmdArgType.DevUChar,
        "None": CmdArgType.DevVoid,
        "state": CmdArgType.DevState,
        "enum": CmdArgType.DevEnum,
        "blob": CmdArgType.DevPipeBlob,
    }

    try:
        ret[long] = ret[int]
    except NameError:
        pass

    for key in dir(CmdArgType):
        if key.startswith("Dev"):
            value = getattr(CmdArgType, key)
            ret[key] = ret[value] = value

    if constants.NUMPY_SUPPORT:
        import numpy

        FROM_TANGO_TO_NUMPY_TYPE = {
            CmdArgType.DevBoolean: numpy.bool8,
            CmdArgType.DevUChar: numpy.ubyte,
            CmdArgType.DevShort: numpy.short,
            CmdArgType.DevUShort: numpy.ushort,
            CmdArgType.DevLong: numpy.int32,
            CmdArgType.DevULong: numpy.uint32,
            CmdArgType.DevLong64: numpy.int64,
            CmdArgType.DevULong64: numpy.uint64,
            CmdArgType.DevString: numpy.str,
            CmdArgType.DevDouble: numpy.float64,
            CmdArgType.DevFloat: numpy.float32,
        }

        for key, value in FROM_TANGO_TO_NUMPY_TYPE.items():
            ret[value] = key

    return ret


_TANGO_TYPE = __build_to_tango_type()


def get_attrs(obj):
    """Helper for dir2 implementation."""
    if not hasattr(obj, "__dict__"):
        return []  # slots only
    proxy_type = types.MappingProxyType
    if not isinstance(obj.__dict__, (dict, proxy_type)):
        print(type(obj.__dict__), obj)
        raise TypeError("%s.__dict__ is not a dictionary" % obj.__name__)
    return obj.__dict__.keys()


def dir2(obj):
    """Default dir implementation.
    Inspired by gist: katyukha/dirmixin.py
    https://gist.github.com/katyukha/c6e5e2b829e247c9b009
    """
    attrs = set()

    if not hasattr(obj, "__bases__"):
        # obj is an instance
        if not hasattr(obj, "__class__"):
            # slots
            return sorted(get_attrs(obj))
        klass = obj.__class__
        attrs.update(get_attrs(klass))
    else:
        # obj is a class
        klass = obj

    for cls in klass.__bases__:
        attrs.update(get_attrs(cls))
        attrs.update(dir2(cls))
    attrs.update(get_attrs(obj))
    return list(attrs)


# TODO IMPLEMENT IT
def get_enum_labels():
    pass


def EnumTypeError():
    pass


_scalar_int_types = (
    CmdArgType.DevShort,
    CmdArgType.DevUShort,
    CmdArgType.DevInt,
    CmdArgType.DevLong,
    CmdArgType.DevULong,
    CmdArgType.DevLong64,
    CmdArgType.DevULong64,
)

_scalar_float_types = (CmdArgType.DevFloat, CmdArgType.DevDouble)

_scalar_numerical_types = _scalar_int_types + _scalar_float_types

_scalar_str_types = (CmdArgType.DevString, CmdArgType.ConstDevString)

_scalar_bool_types = (CmdArgType.DevBoolean,)

_scalar_types = (
    _scalar_numerical_types
    + _scalar_str_types
    + _scalar_bool_types
    + (CmdArgType.DevEncoded, CmdArgType.DevUChar)
)

_array_int_types = (
    CmdArgType.DevVarShortArray,
    CmdArgType.DevVarUShortArray,
    CmdArgType.DevVarLongArray,
    CmdArgType.DevVarULongArray,
    CmdArgType.DevVarLong64Array,
    CmdArgType.DevVarULong64Array,
)

_array_float_types = (CmdArgType.DevVarFloatArray, CmdArgType.DevVarDoubleArray)

_array_numerical_types = _array_int_types + _array_float_types

_array_str_types = (CmdArgType.DevVarStringArray,)

_array_bool_types = (CmdArgType.DevVarBooleanArray,)

_array_types = (
    _array_numerical_types
    + _array_bool_types
    + _array_str_types
    + (
        CmdArgType.DevVarCharArray,
        CmdArgType.DevVarDoubleStringArray,
        CmdArgType.DevVarLongStringArray,
    )
)

_binary_types = (CmdArgType.DevEncoded, CmdArgType.DevVarCharArray)


TO_TANGO_TYPE = __build_to_tango_type()

_scalar_to_array_type = {
    CmdArgType.DevBoolean: CmdArgType.DevVarBooleanArray,
    CmdArgType.DevUChar: CmdArgType.DevVarCharArray,
    CmdArgType.DevShort: CmdArgType.DevVarShortArray,
    CmdArgType.DevUShort: CmdArgType.DevVarUShortArray,
    CmdArgType.DevInt: CmdArgType.DevVarLongArray,
    CmdArgType.DevLong: CmdArgType.DevVarLongArray,
    CmdArgType.DevULong: CmdArgType.DevVarULongArray,
    CmdArgType.DevLong64: CmdArgType.DevVarLong64Array,
    CmdArgType.DevULong64: CmdArgType.DevVarULong64Array,
    CmdArgType.DevFloat: CmdArgType.DevVarFloatArray,
    CmdArgType.DevDouble: CmdArgType.DevVarDoubleArray,
    CmdArgType.DevString: CmdArgType.DevVarStringArray,
    CmdArgType.ConstDevString: CmdArgType.DevVarStringArray,
}


def is_str(tg_type, inc_array=False):
    """Tells if the given tango type is string
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :param inc_array: (optional, default is False) determines if include array
                      in the list of checked types
    :type inc_array: :py:obj:`bool`
    :return: True if the given tango type is string or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _scalar_str_types, _array_str_types
    if tg_type in _scalar_str_types:
        return True
    if not inc_array:
        return False
    return tg_type in _array_str_types


is_str_type = is_str


###############
try:
    __str_klasses = (basestring,)
except NameError:
    __str_klasses = (str,)

__int_klasses = (int,)
__number_klasses = (numbers.Number,)
__seq_klasses = collections_abc.Sequence, bytearray

__use_unicode = False
try:
    unicode
    __use_unicode = True
    __str_klasses = tuple(list(__str_klasses) + [unicode])
except NameError:
    pass

__use_long = False
try:
    long
    __use_long = True
    __int_klasses = tuple(list(__int_klasses) + [long])
except NameError:
    pass

if constants.NUMPY_SUPPORT:
    import numpy

    __int_klasses = tuple(list(__int_klasses) + [numpy.integer])
    __number_klasses = tuple(list(__number_klasses) + [numpy.number])
    __seq_klasses = tuple(list(__seq_klasses) + [numpy.ndarray])

__str_klasses = tuple(__str_klasses)
__int_klasses = tuple(__int_klasses)
__number_klasses = tuple(__number_klasses)
__seq_klasses = tuple(__seq_klasses)


def is_pure_str(obj):
    """
    Tells if the given object is a python string.
    In python 2.x this means any subclass of basestring.
    In python 3.x this means any subclass of str.
    :param obj: the object to be inspected
    :type obj: :py:obj:`object`
    :return: True is the given obj is a string or False otherwise
    :rtype: :py:obj:`bool`
    """
    return isinstance(obj, __str_klasses)


def is_seq(obj):
    """
    Tells if the given object is a python sequence.
    It will return True for any collections.Sequence (list, tuple,
    str, bytes, unicode), bytearray and (if numpy is enabled)
    numpy.ndarray
    :param obj: the object to be inspected
    :type obj: :py:obj:`object`
    :return: True is the given obj is a sequence or False otherwise
    :rtype: :py:obj:`bool`
    """
    return isinstance(obj, __seq_klasses)


def is_non_str_seq(obj):
    """
    Tells if the given object is a python sequence (excluding string
    sequences).
    It will return True for any collections.Sequence (list, tuple (and
    bytes in python3)), bytearray and (if numpy is enabled)
    numpy.ndarray
    :param obj: the object to be inspected
    :type obj: :py:obj:`object`
    :return: True is the given obj is a sequence or False otherwise
    :rtype: :py:obj:`bool`
    """
    return is_seq(obj) and not is_pure_str(obj)


def is_integer(obj):
    """
    Tells if the given object is a python integer.
    It will return True for any int, long (in python 2) and
    (if numpy is enabled) numpy.integer
    :param obj: the object to be inspected
    :type obj: :py:obj:`object`
    :return:
        True is the given obj is a python integer or False otherwise
    :rtype: :py:obj:`bool`
    """
    return isinstance(obj, __int_klasses)


def is_number(obj):
    """
    Tells if the given object is a python number.
    It will return True for any numbers.Number and (if numpy is
    enabled) numpy.number
    :param obj: the object to be inspected
    :type obj: :py:obj:`object`
    :return:
        True is the given obj is a python number or False otherwise
    :rtype: :py:obj:`bool`
    """
    return isinstance(obj, __number_klasses)


def is_scalar(tg_type):
    """Tells if the given tango type is a scalar
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :return: True if the given tango type is a scalar or False otherwise
    :rtype: :py:obj:`bool`
    """

    global _scalar_types
    return tg_type in _scalar_types


is_scalar_type = is_scalar


def is_array(tg_type):
    """Tells if the given tango type is an array type
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :return: True if the given tango type is an array type or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _array_types
    return tg_type in _array_types


is_array_type = is_array


def is_numerical(tg_type, inc_array=False):
    """Tells if the given tango type is numerical
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :param inc_array: (optional, default is False) determines if include array
                      in the list of checked types
    :type inc_array: :py:obj:`bool`
    :return: True if the given tango type is a numerical or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _scalar_numerical_types, _array_numerical_types
    if tg_type in _scalar_numerical_types:
        return True
    if not inc_array:
        return False
    return tg_type in _array_numerical_types


is_numerical_type = is_numerical


def is_int(tg_type, inc_array=False):
    """Tells if the given tango type is integer
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :param inc_array: (optional, default is False) determines if include array
                      in the list of checked types
    :type inc_array: :py:obj:`bool`
    :return: True if the given tango type is integer or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _scalar_int_types, _array_int_types
    if tg_type in _scalar_int_types:
        return True
    if not inc_array:
        return False
    return tg_type in _array_int_types


is_int_type = is_int


def is_float(tg_type, inc_array=False):
    """Tells if the given tango type is float
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :param inc_array: (optional, default is False) determines if include array
                      in the list of checked types
    :type inc_array: :py:obj:`bool`
    :return: True if the given tango type is float or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _scalar_float_types, _array_float_types
    if tg_type in _scalar_float_types:
        return True
    if not inc_array:
        return False
    return tg_type in _array_float_types


is_float_type = is_float


def is_bool(tg_type, inc_array=False):
    """Tells if the given tango type is boolean
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :param inc_array: (optional, default is False) determines if include array
                      in the list of checked types
    :type inc_array: :py:obj:`bool`
    :return: True if the given tango type is boolean or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _scalar_bool_types, _array_bool_types
    if tg_type in _scalar_bool_types:
        return True
    if not inc_array:
        return False
    return tg_type in _array_bool_types


is_bool_type = is_bool


def is_str(tg_type, inc_array=False):
    """Tells if the given tango type is string
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :param inc_array: (optional, default is False) determines if include array
                      in the list of checked types
    :type inc_array: :py:obj:`bool`
    :return: True if the given tango type is string or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _scalar_str_types, _array_str_types
    if tg_type in _scalar_str_types:
        return True
    if not inc_array:
        return False
    return tg_type in _array_str_types


is_str_type = is_str


def is_binary(tg_type, inc_array=False):
    """Tells if the given tango type is binary
    :param tg_type: tango type
    :type tg_type: :class:`tango.CmdArgType`
    :param inc_array: (optional, default is False) determines if include array
                      in the list of checked types
    :type inc_array: :py:obj:`bool`
    :return: True if the given tango type is binary or False otherwise
    :rtype: :py:obj:`bool`
    """
    global _binary_types
    return tg_type in _binary_types


is_binary_type = is_binary
