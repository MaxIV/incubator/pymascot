from mascot.attribute import attribute


class DeviceMeta(type):
    """ Device Metaclass, maybe the attribute name and fget shall not be handle here  """

    def __new__(cls, name, bases, dct):
        attr_dct = {}
        for k, attr in dct.items():
            # Lets get all attributes and set there name
            if isinstance(attr, attribute):
                attr._name = k
                fget = attr._fget
                # Fget is not specified, let use the default one
                if not fget:
                    fget = "read_{}".format(k)
                attr._fget = fget
                # Remove it from main cls ?
                # TODO : Move it after base (for inheritance)
                attr_dct[k] = attr

        base_dct = {}
        for base in reversed(bases):
            for key, value in base.__dict__.items():
                if isinstance(value, attribute):
                    base_dct[key] = value
                    attr_dct[key] = value
        base_dct.update(dct)
        base_dct["_attr_map"] = attr_dct
        return super().__new__(cls, name, bases, base_dct)
