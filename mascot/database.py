import os
from fnmatch import fnmatch
import grpc

from mascot.grpcproto.database_pb2 import DbDevExportInfo, DeviceName
from mascot.grpcproto.database_pb2_grpc import DirectoryStub

from mascot_registry.client_registry import ClientRegistry
from mascot.dataclasses import DbDevInfo, DbDevFullInfo


# TODO: not used anymore
class DatabaseClient:
    def __init__(self, database_host=None):
        if not database_host:
            database_host = os.getenv["TANGO_HOST"]

        self.host = database_host
        self.channel = grpc.insecure_channel(self.host)
        self.directory = DirectoryStub(self.channel)

    def export_device(self, device_name, host, port):
        dev_info = DbDevExportInfo()
        dev_info.name = device_name
        dev_info.address = "{}:{}".format(host, port)
        dev_info.host = host
        dev_info.version = "345"
        dev_info.pid = os.getpid()
        reply = self.directory.ExportDevice(dev_info)
        return reply

    def import_device(self, device_name):
        ds_name = DeviceName(name=device_name)
        return self.directory.ImportDevice(ds_name)


class TangoDatabase:
    def __init__(self, db=None):
        self.database_registry = ClientRegistry(db)

    # def __getattribute__(self, name):
    #     import inspect
    #     returned = object.__getattribute__(self, name)
    #     if inspect.isfunction(returned) or inspect.ismethod(returned):
    #         print('called ', returned.__name__)
    #     return returned

    def _add_server(self, *args, **kwargs):
        pass

    def _db_cache(self, *args, **kwargs):
        pass

    def _delete_class_attribute_property(self, *args, **kwargs):
        pass

    def _delete_class_property(self, *args, **kwargs):
        pass

    def _delete_device_attribute_property(self, *args, **kwargs):
        pass

    def _delete_device_property(self, *args, **kwargs):
        pass

    def _delete_property(self, *args, **kwargs):
        pass

    def _export_server(self, *args, **kwargs):
        pass

    def _get_class_attribute_property(self, *args, **kwargs):
        pass

    def _get_class_property(self, *args, **kwargs):
        pass

    def _get_device_attribute_property(self, *args, **kwargs):
        pass

    def _get_device_property(self, *args, **kwargs):
        pass

    def _get_device_property_list(self, *args, **kwargs):
        pass

    def _get_property(self, *args, **kwargs):
        pass

    def _get_property_forced(self, *args, **kwargs):
        pass

    def _put_class_attribute_property(self, *args, **kwargs):
        pass

    def _put_class_property(self, *args, **kwargs):
        pass

    def _put_device_attribute_property(self, *args, **kwargs):
        pass

    def _put_device_property(self, *args, **kwargs):
        pass

    def _put_property(self, *args, **kwargs):
        pass

    def add_device(self, dev_info: DbDevInfo):
        """
        add_device(self, dev_info) -> None

                Add a device to the database. The device name, server and class
                are specified in the DbDevInfo structure

                Example :
                    dev_info = DbDevInfo()
                    dev_info.name = 'my/own/device'
                    dev_info._class = 'MyDevice'
                    dev_info.server = 'MyServer/test'
                    db.add_device(dev_info)

            Parameters :
                - dev_info : (DbDevInfo) device information
            Return     : None
        Type:      instancemethod

        """
        mascotclass = dev_info.klass
        server = dev_info.server
        device = dev_info.name
        self.database_registry.CreateDevice(
            device=device, mascotclass=mascotclass, server=server
        )
        pass

    def add_server(self, *args, **kwargs):
        pass

    def build_connection(self, *args, **kwargs):
        pass

    def cancel_all_polling_asynch_request(self, *args, **kwargs):
        pass

    def cancel_asynch_request(self, *args, **kwargs):
        pass

    def check_access_control(self, *args, **kwargs):
        pass

    def check_tango_host(self, *args, **kwargs):
        pass

    def command_inout(self, *args, **kwargs):
        pass

    def command_inout_asynch(self, *args, **kwargs):
        pass

    def command_inout_raw(self, *args, **kwargs):
        pass

    def command_inout_reply(self, *args, **kwargs):
        pass

    def command_inout_reply_raw(self, *args, **kwargs):
        pass

    def connect(self, *args, **kwargs):
        pass

    def defaultCommandExtractAs(self, *args, **kwargs):
        pass

    def delete_attribute_alias(self, *args, **kwargs):
        pass

    def delete_class_attribute_property(self, *args, **kwargs):
        pass

    def delete_class_property(self, *args, **kwargs):
        pass

    def delete_device(self, *args, **kwargs):
        pass

    def delete_device_alias(self, *args, **kwargs):
        pass

    def delete_device_attribute_property(self, *args, **kwargs):
        pass

    def delete_device_property(self, *args, **kwargs):
        # TODO: Needed for  webjive
        pass

    def delete_property(self, *args, **kwargs):
        pass

    def delete_server(self, *args, **kwargs):
        pass

    def delete_server_info(self, *args, **kwargs):
        pass

    def dev_name(self, *args, **kwargs):
        pass

    def export_device(self, *args, **kwargs):
        pass

    def export_event(self, *args, **kwargs):
        pass

    def export_server(self, *args, **kwargs):
        pass

    def get_access_control(self, *args, **kwargs):
        pass

    def get_access_except_errors(self, *args, **kwargs):
        pass

    def get_access_right(self, *args, **kwargs):
        pass

    def get_alias(self, *args, **kwargs):
        pass

    def get_alias_from_attribute(self, *args, **kwargs):
        pass

    def get_alias_from_device(self, *args, **kwargs):
        pass

    def get_asynch_replies(self, *args, **kwargs):
        pass

    def get_attribute_alias(self, *args, **kwargs):
        pass

    def get_attribute_alias_list(self, *args, **kwargs):
        pass

    def get_attribute_from_alias(self, *args, **kwargs):
        pass

    def get_class_attribute_list(self, *args, **kwargs):
        pass

    def get_class_attribute_property(self, *args, **kwargs):
        pass

    def get_class_attribute_property_history(self, *args, **kwargs):
        pass

    def get_class_for_device(self, *args, **kwargs):
        pass

    def get_class_inheritance_for_device(self, *args, **kwargs):
        pass

    def get_class_list(self, *args, **kwargs):
        pass

    def get_class_property(self, *args, **kwargs):
        pass

    def get_class_property_history(self, *args, **kwargs):
        pass

    def get_class_property_list(self, *args, **kwargs):
        pass

    def get_db_host(self, *args, **kwargs):
        pass

    def get_db_port(self, *args, **kwargs):
        pass

    def get_db_port_num(self, *args, **kwargs):
        pass

    def get_dev_host(self, *args, **kwargs):
        pass

    def get_dev_port(self, *args, **kwargs):
        pass

    def get_device_alias(self, *args, **kwargs):
        pass

    def get_device_alias_list(self, *args, **kwargs):
        pass

    def get_device_attribute_property(self, *args, **kwargs):
        pass

    def get_device_attribute_property_history(self, *args, **kwargs):
        pass

    def get_device_class_list(self, server: str) -> list:
        """
            Query the database for a list of devices and classes served by
            the specified server. Return a list with the following structure:
            [device name, class name, device name, class name, ...]
        """
        return [
            d.mascotclass for d in self.database_registry.ListDevices(server).devices
        ]

    def get_device_domain(self, wildcard):
        return self._match_device_index(wildcard, 0)

    def get_device_exported(self, wildcard):
        # TODO: Needed for  webjive
        # TODO: report only exported
        return self._match_device(wildcard)

    def get_device_exported_for_class(self, *args, **kwargs):
        pass

    def _match_device(self, wildcard):
        out = []
        for server in self.get_server_name_list():
            # Unzip
            devs, _ = zip(*self.get_device_class_list(server))
            out += [d for d in devs if fnmatch(d, wildcard)]
        return out

    def _match_device_index(self, wildcard, index):
        out = set()
        for server in self.get_server_name_list():
            # Unzip
            devs, _ = zip(*self.get_device_class_list(server))
            out |= {d.split("/")[index] for d in devs if fnmatch(d, wildcard)}
        return out

    def get_device_family(self, wildcard):
        # TODO: Needed for  webjive
        return self._match_device_index(wildcard, 1)

    def get_device_from_alias(self, *args, **kwargs):
        pass

    def get_device_info(self, dev_name):
        # TODO: Needed for  webjive
        found = False
        for srv in self.get_server_name_list():
            for dev, klass in self.get_device_class_list(srv):
                if dev_name == dev:
                    found = True
                    break
        if not found:
            # TODO raise DB_DeviceNotDefined
            raise Exception(f"DB_DeviceNotDefined {dev_name}")
        return DbDevFullInfo(class_name=klass, ds_full_name=srv, name=dev)

    def get_device_member(self, wildcard):
        return self._match_device_index(wildcard, 2)
        # TODO: Needed for  webjive

    def get_device_name(self, *args, **kwargs):
        pass

    def get_device_property(self, value):
        # TODO: Needed for  webjive
        # TODO Fix
        return {str(value): []}

    def get_device_property_history(self, *args, **kwargs):
        pass

    def get_device_property_list(self, *args, **kwargs):
        # TODO: Needed for  webjive
        pass
        return []

    def get_device_service_list(self, *args, **kwargs):
        pass

    def get_file_name(self, *args, **kwargs):
        pass

    def get_fqdn(self, *args, **kwargs):
        pass

    def get_from_env_var(self, *args, **kwargs):
        pass

    def get_host_list(self, *args, **kwargs):
        pass

    def get_host_server_list(self, *args, **kwargs):
        pass

    def get_idl_version(self, *args, **kwargs):
        pass

    def get_info(self, *args, **kwargs):
        pass

    def get_instance_name_list(self, wildcard):
        # TODO: Needed for  webjive
        instances = []
        for srv in self.get_server_name_list():
            try:
                srv_name, instance = srv.split("/")
                if fnmatch(srv_name, wildcard):
                    instances.append(instance)
            except ValueError:
                pass
        return instances

    def get_object_list(self, *args, **kwargs):
        pass

    def get_object_property_list(self, *args, **kwargs):
        pass

    def get_property(self, *args, **kwargs):
        pass

    def get_property_forced(self, *args, **kwargs):
        pass

    def get_property_history(self, *args, **kwargs):
        pass

    def get_server_class_list(self, *args, **kwargs):
        pass

    def get_server_info(self, *args, **kwargs):
        pass

    def get_server_list(self, *args, **kwargs):
        pass

    def get_server_name_list(self):
        # TODO: Needed for  webjive
        return self.database_registry.ListServers()

    def get_server_release(self, *args, **kwargs):
        pass

    def get_service_list(self, *args, **kwargs):
        pass

    def get_services(self, *args, **kwargs):
        pass

    def get_source(self, *args, **kwargs):
        pass

    def get_timeout_millis(self, *args, **kwargs):
        pass

    def get_transparency_reconnection(self, *args, **kwargs):
        pass

    def import_device(self, *args, **kwargs):
        pass

    def is_control_access_checked(self, *args, **kwargs):
        pass

    def is_dbase_used(self, *args, **kwargs):
        pass

    def is_multi_tango_host(self, *args, **kwargs):
        pass

    def put_attribute_alias(self, *args, **kwargs):
        pass

    def put_class_attribute_property(self, *args, **kwargs):
        pass

    def put_class_property(self, *args, **kwargs):
        pass

    def put_device_alias(self, *args, **kwargs):
        pass

    def put_device_attribute_property(self, *args, **kwargs):
        pass

    def put_device_property(self, *args, **kwargs):
        # TODO: Needed for  webjive
        pass

    def put_property(self, *args, **kwargs):
        pass

    def put_server_info(self, *args, **kwargs):
        pass

    def reconnect(self, *args, **kwargs):
        pass

    def register_service(self, *args, **kwargs):
        pass

    def rename_server(self, *args, **kwargs):
        pass

    def reread_filedatabase(self, *args, **kwargs):
        pass

    def set_access_checked(self, *args, **kwargs):
        pass

    def set_access_control(self, *args, **kwargs):
        pass

    def set_source(self, *args, **kwargs):
        pass

    def set_timeout_millis(self, *args, **kwargs):
        pass

    def set_transparency_reconnection(self, *args, **kwargs):
        pass

    def unexport_device(self, *args, **kwargs):
        pass

    def unexport_event(self, *args, **kwargs):
        pass

    def unexport_server(self, *args, **kwargs):
        pass

    def unregister_service(self, *args, **kwargs):
        pass

    def write_filedatabase(self, *args, **kwargs):
        pass


Database = TangoDatabase
