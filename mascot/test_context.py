from concurrent import futures
from contextlib import contextmanager
from functools import partial

import grpc

from mascot.asyncio import DeviceProxy as asyncio_DeviceProxy
from mascot.device_proxy import DeviceProxy
from mascot.grpc_servicer import DeviceServicer
from mascot.grpcproto.device_server_pb2_grpc import (
    add_MascotDeviceServerServicer_to_server,
)
from mascot.tango_enums import GreenMode
from mascot.runners import runlocaly

GREEN_MODE_DS = {
    GreenMode.Synchronous: DeviceProxy,
    GreenMode.Asyncio: partial(asyncio_DeviceProxy, wait=True),
}


@contextmanager
def DeviceTestContext(
    device_cls,
    device_name="a/b/c",
    server_name="Test",
    host="0.0.0.0",
    port=50001,
    green_mode=GreenMode.Synchronous,
    *args,
    **kwargs,
):
    Proxy = GREEN_MODE_DS[green_mode]
    with runlocaly(device_cls, device_name, server_name, host, port) as server:
        proxy = Proxy(device_name, connection_addr=server._host)
        proxy._mascot_test_context_server = server
        yield proxy
