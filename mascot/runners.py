import logging
import os
import sys
from concurrent import futures
from contextlib import contextmanager
from time import sleep

import grpc

from mascot.grpc_servicer import DeviceServicer
from mascot.grpcproto.device_server_pb2_grpc import (
    add_MascotDeviceServerServicer_to_server,
)
from mascot.utils import get_network_interfaces
from mascot_registry.client_registry import ClientRegistry


class MascotGrpcServer:
    def __init__(self, grpc_server, device_servicer):
        self.grpc_server = grpc_server
        self.device_servicer = device_servicer

    @property
    def _host(self):
        return self.grpc_server._host

    def get_device_list(self):
        return self.device_servicer.device_list.keys()

    def get_device_object(self, name):
        return self.device_servicer.device_list[name]


@contextmanager
def runlocaly(cls, device_name="a/b/c", server_name=None, host="[::]", port=50001):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    device_service = DeviceServicer(server_name)
    device_service.add_device(device_name, cls(device_name))
    add_MascotDeviceServerServicer_to_server(device_service, server)
    host = f"{host}:{port}"
    server.add_insecure_port(host)
    server._host = host
    server.start()
    yield MascotGrpcServer(server, device_service)
    server.stop(0)


def run(cls, server_name=None, ports=[], hosts=[]):
    tango_host = os.getenv("TANGO_HOST")
    db = ClientRegistry(tango_host)
    # create a gRPC server
    if not server_name:
        try:
            server_name = sys.argv[1]
        except IndexError:
            print("A server name is required")
            return
    # Create grpc server
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    # Setup servicer
    device_service = DeviceServicer(server_name)
    devices_list = db.ListDevices(server_name)
    # TODO use database instead
    devices_name = [
        (f"{d.name.domain}/{d.name.family}/{d.name.identifier}", d.mascotclass)
        for d in devices_list.devices
    ]
    # devices_name = ["sys/tg_test/1", "sys/tg_test/2"]
    # Instanciate mascot devices
    for name, cls_name in devices_name:
        # TODO: multiclass support
        if not cls_name == cls.__name__:
            msg = f"Cannot start {name} of class {cls_name} with class {cls}"
            logging.error(msg)
            # TODO: Or raise ?
            return
        logging.info("class {}, device {}".format(cls, name))
        device_service.add_device(name, cls(name))

    # Server servicer
    add_MascotDeviceServerServicer_to_server(device_service, server)
    if not ports and not hosts:
        port = server.add_insecure_port("[::]:0")
        ports.append(port)
        hosts = get_network_interfaces()
        # TODO: handle multi host
        host = hosts[-1]
    else:
        raise NotImplementedError()
    server.start()
    logging.info(f"Starting server for {host}. Listening on port {port}.")
    db.PublishServer(server_name, host, port)
    # Export device:
    # for name in devices_name:
    #     db.PublishDevice()
    logging.basicConfig(level=logging.INFO)
    try:
        while True:
            sleep(86400)
    except KeyboardInterrupt:
        logging.info("Stoping server.")
        server.stop(0)
