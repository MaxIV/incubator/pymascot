from dataclasses import dataclass, field
from typing import Any, List

from mascot.tango_enums import AttrDataFormat
from mascot.tango_enums import AttrMemorizedType
from mascot.tango_enums import AttrQuality
from mascot.tango_enums import AttrWriteType
from mascot.tango_enums import CmdArgType
from mascot.tango_enums import DispLevel
from mascot.utils import value_unpacker


@dataclass
class AttributeAlarmInfo:
    delta_t: str = ""
    delta_val: str = ""
    extensions: List = field(default_factory=list)
    max_alarm: str = ""
    max_warning: str = ""
    min_alarm: str = ""
    min_warning: str = ""


@dataclass
class ArchiveEventInfo:
    archive_abs_change: str = ""
    archive_period: str = ""
    archive_rel_change: str = ""
    extensions: List = field(default_factory=list)


@dataclass
class ChangeEventInfo:
    abs_change: str = ""
    extensions: List = field(default_factory=list)
    rel_change: str = ""


@dataclass
class PeriodicEventInfo:
    extensions: List = field(default_factory=list)
    period: str = ""


@dataclass
class AttributeEventInfo:
    arch_event: ArchiveEventInfo = ArchiveEventInfo()
    ch_event: ChangeEventInfo = ChangeEventInfo()
    per_event: PeriodicEventInfo = PeriodicEventInfo()


@dataclass
class AttributeInfoEx:
    alarms: AttributeAlarmInfo = AttributeAlarmInfo()
    data_format: AttrDataFormat = AttrDataFormat.SCALAR
    data_type: CmdArgType = CmdArgType.DevVoid
    description: str = ""
    disp_level: DispLevel = DispLevel.OPERATOR
    display_unit: str = ""
    enum_labels: List = field(default_factory=list)
    events: AttributeEventInfo = AttributeEventInfo()
    extensionsi: List = field(default_factory=list)
    format: str = ""
    label: str = ""
    max_alarm: str = ""
    max_dim_x: int = 0
    max_dim_y: int = 0
    max_value: str = ""
    memorized: AttrMemorizedType = AttrMemorizedType.NOT_KNOWN
    min_alarm: str = ""
    min_value: str = ""
    name: str = ""
    root_attr_name: str = ""
    standard_unit: str = ""
    sys_extensions: List = field(default_factory=list)
    unit: str = ""
    writable: AttrWriteType = AttrWriteType.READ
    writable_attr_name: str = ""


AttributeInfo = AttributeInfoEx


def build_attribute_info_ex(grpc_attribute_config):
    attr = AttributeInfoEx()
    attr.name = grpc_attribute_config.name
    # TODO: other fileds
    return attr
