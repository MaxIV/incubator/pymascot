from dataclasses import dataclass


# Device


@dataclass
class DeviceInfo:
    dev_class: str = ""
    dev_type: str = ""
    doc_url: str = ""
    server_host: str = ""
    server_id: str = ""
    server_version: int = 0


# Database


@dataclass
class DbDevFullInfo:
    class_name: str = ""
    ds_full_name: str = ""
    exported: int = 0
    ior: str = ""
    name: str = ""
    pid: int = 0
    started_date: str = ""
    stopped_date: str = ""
    version: str = ""


@dataclass
class DbDevInfo:
    klass: str = ""
    name: str = ""
    server: str = ""
