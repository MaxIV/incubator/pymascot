from mascot.grpcproto.attribute_config_pb2 import AttributeConfig
from mascot.grpcproto.attributes_pb2 import DeviceAttribute
from mascot.utils import _TANGO_TYPE, get_digits
from mascot.tango_enums import CmdArgType


class Attribute:
    """ A mascot Attribute"""

    # TODO Merge this with tango.attr_data.py
    # Might be better to split the attribute definition with the attribute
    # used by the device

    def __init__(self, fget=None, **kwargs):
        # Read, write, is_allowed function
        self._kwargs = kwargs
        self._fget = fget
        self._fset = kwargs.get("fset", None)
        self._fisallowed = kwargs.get("fisallowed", None)
        self._value = None
        self._w_value = None
        self._quality = None
        self._timestamp_usec = None
        self._timestamp_sec = None
        self._timestamp_nsec = None
        self._type = kwargs.get("dtype", None)
        self._data_format = 0
        self._name = None
        self._dim_x = kwargs.get("dim_x", 1)
        self._dim_y = kwargs.get("dim_y", 0)
        self.w_dim_x = kwargs.get("dim_x", 1)
        self.w_dim_y = kwargs.get("dim_y", 0)
        self.setup_attribute_config()

    @property
    def writable(self):
        # TODO: Data format is writable type
        return False

    def get_data_type(self):
        if isinstance(self._type, CmdArgType):
            return self._type
        return _TANGO_TYPE[self._type]

    def setup_attribute_config(self):
        # TODO; check atribute constructor and verify kwarg names
        kwargs = self._kwargs
        self._memorized = kwargs.get("memorized", False)
        self._description = ""  # TODO: get doc string
        self._mem_init = False  # ????
        self._label = self._kwargs.get("label", "")
        self._unit = kwargs.get("unit", "")
        self._standard_unit = kwargs.get("standard_unit", "")
        self._display_unit = kwargs.get("display_unit", "")
        self._format = kwargs.get("format", "")
        self._min_value = kwargs.get("min_value", "")
        self._max_value = kwargs.get("max_value", "")
        self._level = kwargs.get("level", "")

    def build_attribute_config_reply(self):
        attr_cfg = AttributeConfig()
        attr_cfg.name = self._name
        attr_cfg.writable = self.writable
        attr_cfg.data_format = self._data_format
        attr_cfg.data_type = 1
        attr_cfg.memorized = False
        attr_cfg.mem_init = False
        attr_cfg.max_dim_x = self._dim_x
        attr_cfg.max_dim_y = self._dim_y
        attr_cfg.description = self._description
        attr_cfg.label = self._label
        attr_cfg.unit = self._unit
        attr_cfg.standard_unit = self._standard_unit
        attr_cfg.display_unit = self._display_unit
        attr_cfg.format = self._format
        attr_cfg.min_value = self._min_value
        attr_cfg.max_value = self._max_value
        # TODO not implemented
        # attr_cfg.writable_attr_name = ""
        # attr_cfg.level = 1
        # attr_cfg.root_attr_name =  ""
        # attr_cfg.enum_labels = ""
        # attr_cfg.att_alarm = 21;
        # attr_cfg.event_prop = 22;
        # attr_cfg.extensions = 23;
        # attr_cfg.sys_extensions = 24;
        return attr_cfg

    def set_timestamp(self, time):
        """ Set the attribute time stamp (sec usec nsec) from unix timestamp"""
        self._timestamp_sec = int(time)
        self._timestamp_usec = get_digits(time, 0, 6)
        self._timestamp_nsec = get_digits(time, 0, 9)

    def build_attribute_reply(self):
        """ Represent this attribute to a grpc reply"""
        # Build the reply
        device_attr = DeviceAttribute()
        # TODO: Remove hardcoded values
        device_attr.data_format = 0  # Scalar
        device_attr.dim_x = 1
        device_attr.dim_y = 0
        device_attr.has_failed = False
        device_attr.is_empty = False
        device_attr.name = self._name
        device_attr.nb_read = 1
        device_attr.nb_written = 1
        device_attr.quality = self._quality.value
        # r_dimension
        device_attr.r_dimension.dim_x = 1
        device_attr.r_dimension.dim_y = 0
        # time
        device_attr.time.tv_nsec = self._timestamp_nsec
        device_attr.time.tv_sec = self._timestamp_sec
        device_attr.time.tv_usec = self._timestamp_usec
        device_attr.type = self.get_data_type().value  # Double
        if self._type == int:
            device_attr.value.intValue = int(self._value)
        if self._type == bool:
            device_attr.value.boolValue = bool(self._value)
        if self._type == float:
            device_attr.value.doubleValue = float(self._value)
        if self._type == str:
            device_attr.value.stringValue = str(self._value)
        device_attr.w_dim_x = 1
        device_attr.w_dim_y = 0
        device_attr.w_dimension.dim_x = 1
        device_attr.w_dimension.dim_y = 0
        device_attr.w_value = str(1)
        return device_attr


attribute = Attribute
