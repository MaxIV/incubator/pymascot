import functools
from dataclasses import dataclass
from mascot.tango_enums import DispLevel, CmdArgType


@dataclass
class CommandInfo:
    cmd_name: str = ""
    cmd_tag: int = 0
    disp_level: DispLevel = DispLevel.OPERATOR
    in_type: CmdArgType = CmdArgType.DevVoid
    in_type_desc: str = ""
    out_type: CmdArgType = CmdArgType.DevVoid
    out_type_desc: str = ""


class Command:
    def __init__(
        self,
        f=None,
        dtype_in=None,
        dformat_in=None,
        doc_in="",
        dtype_out=None,
        dformat_out=None,
        doc_out="",
        display_level=None,
        polling_period=None,
        green_mode=None,
    ):
        self.func_call = f
        self.dtype_in = dtype_in
        self.dtype_out = dtype_out
        self.dformat_in = dformat_in
        self.dformat_out = dformat_out
        self.doc_in = doc_in
        self.doc_ut = doc_out
        self.display_level = display_level
        self.polling_period = polling_period
        self.green_mode = green_mode

    @property
    def name(self):
        return self.f.__name__

    def __call__(self, *args, **kwargs):
        return self.func_call(*args, **kwargs)


def command(
    f=None,
    dtype_in=None,
    dformat_in=None,
    doc_in="",
    dtype_out=None,
    dformat_out=None,
    doc_out="",
    display_level=None,
    polling_period=None,
    green_mode=None,
):
    if f is None:
        return functools.partial(
            command,
            dtype_in=dtype_in,
            dformat_in=dformat_in,
            doc_in=doc_in,
            dtype_out=dtype_out,
            dformat_out=dformat_out,
            doc_out=doc_out,
            display_level=display_level,
            polling_period=polling_period,
            green_mode=green_mode,
        )
    else:
        return Command(
            f=f,
            dtype_in=dtype_in,
            dformat_in=dformat_in,
            doc_in=doc_in,
            dtype_out=dtype_out,
            dformat_out=dformat_out,
            doc_out=doc_out,
            display_level=display_level,
            polling_period=polling_period,
            green_mode=green_mode,
        )
