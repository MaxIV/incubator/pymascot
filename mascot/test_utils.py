from mascot.test_context import DeviceTestContext
import numpy
import collections.abc as collections_abc
import pytest


def assert_close(a, b):
    # TODO maybe str is not enought, shall cover all six.string_types
    if isinstance(a, str):
        assert a == b
        return
    if isinstance(a, collections_abc.Sequence) and len(a) and isinstance(a[0], str):
        assert list(a) == list(b)
        return
    try:
        assert a == pytest.approx(b)
    except ValueError:
        numpy.testing.assert_allclose(a, b)


# TODO
# Pytest fixtures
from mascot import DevState
from mascot.server import Device
import enum
from mascot import GreenMode

# char \x00 cannot be sent in a DevString. All other 1-255 chars can
ints = tuple(range(1, 256))
bytes_devstring = bytes(ints)
str_devstring = bytes_devstring.decode("latin-1")

# Test devices


class SimpleDevice(Device):
    def init_device(self):
        self.set_state(DevState.ON)


# Test enums


class GoodEnum(enum.IntEnum):
    START = 0
    MIDDLE = 1
    END = 2


class BadEnumNonZero(enum.IntEnum):
    START = 1
    MIDDLE = 2
    END = 3


class BadEnumSkipValues(enum.IntEnum):
    START = 0
    MIDDLE = 2
    END = 4


class BadEnumDuplicates(enum.IntEnum):
    START = 0
    MIDDLE = 1
    END = 1


TYPED_VALUES = {
    int: (1, 2, -65535, 23),
    float: (2.71, 3.14, -34.678e-10, 12.678e15),
    str: ("hey hey", "my my", bytes_devstring, str_devstring),
    bool: (False, True, True, False),
    (int,): ([1, 2, 3], [9, 8, 7], [-65535, 2224], [0, 0]),
    (float,): ([0.1, 0.2, 0.3], [0.9, 0.8, 0.7], [-6.3232e-3], [0.0, 12.56e12]),
    (str,): (
        ["ab", "cd", "ef"],
        ["gh", "ij", "kl"],
        10 * [bytes_devstring],
        10 * [str_devstring],
    ),
    (bool,): ([False, False, True], [True, False, False], [False], [True]),
}


def repr_type(x):
    if not isinstance(x, tuple):
        return x.__name__
    return "({},)".format(x[0].__name__)


def create_result(dtype, value):
    if dtype == str:
        if isinstance(value, bytes):
            return value.decode("latin-1")
    elif dtype == (str,):
        return [create_result(str, v) for v in value]
    return value


# TODO: Change thie
@pytest.fixture(params=["RUNNING"])
def state(request):
    return request.param


@pytest.fixture(params=list(TYPED_VALUES.items()), ids=lambda x: repr_type(x[0]))
def typed_values(request):
    dtype, values = request.param
    expected = lambda v: create_result(dtype, v)
    return dtype, values, expected


@pytest.fixture(params=GreenMode.values.values())
def green_mode(request):
    return request.param


@pytest.fixture(params=[GreenMode.Synchronous, GreenMode.Asyncio, GreenMode.Gevent])
def server_green_mode(request):
    return request.param
