from enum import IntEnum as _Enum
from mascot.grpcproto import tango_enums_pb2


class Enum(_Enum):
    def __str__(self):
        return _Enum.__str__(self).split(".")[1]


class GreenMode(Enum):
    Synchronous = 0
    Futures = 1
    Gevent = 2
    Asyncio = 3


class ExtractAs(Enum):
    Numpy = 0
    ByteArray = 1
    Bytes = 2
    Tuple = 3
    List = 4
    String = 5
    Nothing = 6


class AttrQuality(Enum):
    ATTR_VALID = tango_enums_pb2.ATTR_VALID
    ATTR_INVALID = tango_enums_pb2.ATTR_INVALID
    ATTR_ALARM = tango_enums_pb2.ATTR_ALARM
    ATTR_CHANGING = tango_enums_pb2.ATTR_CHANGING
    ATTR_WARNING = tango_enums_pb2.ATTR_WARNING


class DevState(Enum):
    ON = tango_enums_pb2.ON
    OFF = tango_enums_pb2.OFF
    CLOSE = tango_enums_pb2.CLOSE
    OPEN = tango_enums_pb2.OPEN
    INSERT = tango_enums_pb2.INSERT
    EXTRACT = tango_enums_pb2.EXTRACT
    MOVING = tango_enums_pb2.MOVING
    STANDBY = tango_enums_pb2.STANDBY
    FAULT = tango_enums_pb2.FAULT
    INIT = tango_enums_pb2.INIT
    RUNNING = tango_enums_pb2.RUNNING
    ALARM = tango_enums_pb2.ALARM
    DISABLE = tango_enums_pb2.DISABLE
    UNKNOWN = tango_enums_pb2.UNKNOWN


class CmdArgType(Enum):
    DevVoid = tango_enums_pb2.DevVoid
    DevBoolean = tango_enums_pb2.DevBoolean
    DevShort = tango_enums_pb2.DevShort
    DevLong = tango_enums_pb2.DevLong
    DevFloat = tango_enums_pb2.DevFloat
    DevDouble = tango_enums_pb2.DevDouble
    DevUShort = tango_enums_pb2.DevUShort
    DevULong = tango_enums_pb2.DevULong
    DevString = tango_enums_pb2.DevString
    DevVarCharArray = tango_enums_pb2.DevVarCharArray
    DevVarShortArray = tango_enums_pb2.DevVarShortArray
    DevVarLongArray = tango_enums_pb2.DevVarLongArray
    DevVarFloatArray = tango_enums_pb2.DevVarFloatArray
    DevVarDoubleArray = tango_enums_pb2.DevVarDoubleArray
    DevVarUShortArray = tango_enums_pb2.DevVarUShortArray
    DevVarULongArray = tango_enums_pb2.DevVarULongArray
    DevVarStringArray = tango_enums_pb2.DevVarStringArray
    DevVarLongStringArray = tango_enums_pb2.DevVarLongStringArray
    DevVarDoubleStringArray = tango_enums_pb2.DevVarDoubleStringArray
    DevState = tango_enums_pb2.DevStates
    ConstDevString = tango_enums_pb2.ConstDevString
    DevVarBooleanArray = tango_enums_pb2.DevVarBooleanArray
    DevUChar = tango_enums_pb2.DevUChar
    DevLong64 = tango_enums_pb2.DevLong64
    DevULong64 = tango_enums_pb2.DevULong64
    DevVarLong64Array = tango_enums_pb2.DevVarLong64Array
    DevVarULong64Array = tango_enums_pb2.DevVarULong64Array
    DevInt = tango_enums_pb2.DevInt
    DevEncoded = tango_enums_pb2.DevEncoded
    DevEnum = tango_enums_pb2.DevEnum
    DevPipeBlob = tango_enums_pb2.DevPipeBlob


# TODO: Do it better !
# Tmp workarround fix for webjive compatibility:
#      PyTango.CmdArgType.values[self._get_attr_info().data_type]
# In this case data_type is an integer and PyTango enums as a value attribute
# to get the object from the enum name
# In our case data_type is directly the enum name
# Here a patch to handle both cases
class EnumDictPatcher(dict):
    def __getitem__(self, item):
        # Item is an iteger, return the corresponding name
        if isinstance(item, int):
            return dict.__getitem__(self, item)
        else:
            # Item is already a name, if it is define just return it
            if item in self.values() or (
                isinstance(item, Enum) and item.name in self.values()
            ):
                return item
            else:
                raise KeyError(f"{item}")


CmdArgType.values = EnumDictPatcher({i.value: i.name for i in CmdArgType})
GreenMode.values = EnumDictPatcher({i.value: i.name for i in GreenMode})


class AttrWriteType(Enum):
    READ = tango_enums_pb2.READ
    READ_WITH_WRITE = tango_enums_pb2.READ_WITH_WRITE
    WRITE = tango_enums_pb2.WRITE
    READ_WRITE = tango_enums_pb2.READ_WRITE


class AttrDataFormat(Enum):
    SCALAR = tango_enums_pb2.SCALAR
    SPECTRUM = tango_enums_pb2.SPECTRUM
    IMAGE = tango_enums_pb2.IMAGE
    FMT_UNKNOWN = tango_enums_pb2.FMT_UNKNOWN


class DispLevel(Enum):
    OPERATOR = tango_enums_pb2.OPERATOR
    EXPERT = tango_enums_pb2.EXPERT


class AttrMemorizedType(Enum):
    NOT_KNOWN = 0
    NONE = 1
    MEMORIZED = 2
    MEMORIZED_WRITE_INIT = 3
