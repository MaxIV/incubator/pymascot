class DevFailed(Exception):
    pass


class DeviceUnlocked(Exception):
    pass


class CommunicationFailed(Exception):
    pass


class ConnectionFailed(Exception):
    pass
