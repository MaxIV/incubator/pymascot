import os

import grpc
from mascot.attribute_config import build_attribute_info_ex
from mascot.dataclasses import DeviceInfo
from mascot.device_attribute import DeviceAttribute
from mascot.errors import DevFailed
from mascot.green import get_green_mode, green
from mascot.grpcproto.attributes_pb2 import ReadAttributeMessage, WriteAttributeMessage
from mascot.grpcproto.commands_pb2 import CommandMessage
from mascot.grpcproto.device_pb2 import GetAttributeListMessage
from mascot.grpcproto.device_server_pb2_grpc import (
    MascotDeviceServerStub as GrpcDeviceProxy,
)
from mascot.tango_enums import GreenMode
from mascot.utils import dir2
from mascot_registry.client_registry import ClientRegistry


class GrpcClient:
    def __init__(self, host_addr):
        self.host_addr = host_addr
        self.channel = grpc.insecure_channel(self.host_addr)
        self.proxy = GrpcDeviceProxy(self.channel)

    def read_attribute(self, device, attr):
        attr = ReadAttributeMessage(deviceName=device, attributeName=attr)
        response = self.proxy.ReadAttribute(attr)
        return response

    def write_attribute(self, device, attr, value):
        attr = WriteAttributeMessage(deviceName=device, attributeName=attr)
        if isinstance(value, int):
            attr.value.intValue = int(value)
        elif isinstance(value, bool):
            attr.value.boolValue = bool(value)
        elif isinstance(value, float):
            attr.value.doubleValue = float(value)
        elif isinstance(value, str):
            attr.value.stringValue = str(value)
        response = self.proxy.WriteAttribute(attr)
        return response

    def command_inout(self, device, command):
        msg = CommandMessage(deviceName=device, commandName=command)
        response = self.proxy.ExecuteCommand(msg)
        return response

    def read_attribute_config(self, device, attr):
        attr = ReadAttributeMessage(deviceName=device, attributeName=attr)
        response = self.proxy.ReadAttributeConfig(attr)
        return response

    def get_attribute_list(self, device):
        request = GetAttributeListMessage(deviceName=device)
        return self.proxy.GetAttributeList(request).attributes


@green(consume_green_mode=False)
def get_device_proxy(*args, **kwargs):
    return DeviceProxy(*args, **kwargs)


class DeviceProxy:

    # def __getattribute__(self, name):
    #     import inspect
    #     returned = object.__getattribute__(self, name)
    #     if inspect.isfunction(returned) or inspect.ismethod(returned):
    #         print('DeviceProxy called ', returned.__name__)
    #     return returned

    def __init__(self, name, connection_addr=None, *args, **kwargs):
        self.__init_device_proxy_internals()
        self._name = name
        # Get info to from the database and store
        # NotImplemented
        self.__server_address = None
        if connection_addr:
            self.__server_address = connection_addr
        self.__grpc_client = None

        self._green_mode = kwargs.pop("green_mode", None)
        self._executors[GreenMode.Futures] = kwargs.pop("executor", None)
        self._executors[GreenMode.Gevent] = kwargs.pop("threadpool", None)
        self._executors[GreenMode.Asyncio] = kwargs.pop("asyncio_executor", None)

    def _state(self):
        return 1

    def __init_device_proxy_internals(self):
        if self.__dict__.get("_initialized", False):
            return
        executors = dict((key, None) for key in GreenMode)
        self.__dict__["_green_mode"] = None
        self.__dict__["_initialized"] = True
        self.__dict__["_executors"] = executors
        self.__dict__["_pending_unsubscribe"] = {}

    def get_green_mode(self):
        gm = self._green_mode
        if gm is None:
            gm = get_green_mode()
        return gm

    def set_green_mode(self, green_mode=None):
        self._green_mode = green_mode

    def __dir__(self):
        extra_entries = set()
        # Add attributes
        try:
            extra_entries.update(self.get_attribute_list())
        except Exception:
            pass
        # Merge with default dir implementation
        extra_entries.update([x.lower() for x in extra_entries])
        entries = extra_entries.union(dir2(self))
        return sorted(entries)

    ###########

    def __refresh_attr_cache(self):
        attr_list = self.get_attribute_list()
        attr_cache = {}
        for attr in attr_list:
            name = attr.lower()
            attr_cache[name] = attr
        self.__dict__["__attr_cache"] = attr_cache

    def __get_attr_cache(self):
        try:
            ret = self.__dict__["__attr_cache"]
        except KeyError:
            self.__dict__["__attr_cache"] = ret = {}
        return ret

    def __getattr_attribute(self, name, update_cache=False):
        if update_cache:
            try:
                self.__refresh_attr_cache()
            except Exception:
                pass
        name_l = name.lower()
        attr_info = self.__get_attr_cache().get(name_l)
        if attr_info:
            return self.read_attribute(name)

    def __getattr__(self, name):
        attr = self.__getattr_attribute(name)
        if not attr:
            attr = self.__getattr_attribute(name, update_cache=True)
        if attr:
            return attr.value
        raise AttributeError(name)

    def __setattr_attribute_value(self, name, value):
        attr_info = self.__get_attr_cache().get(name.lower())
        if attr_info:
            self.write_attribute(name, value)

    def __setattr__(self, name, value):
        name_l = name.lower()
        if name_l in self.__get_attr_cache():
            return self.__setattr_attribute_value(name, value)
        return super(DeviceProxy, self).__setattr__(name, value)

    @property
    def _server_address(self):
        if not self.__server_address:
            # TODO use database instead
            db = ClientRegistry(os.getenv("TANGO_HOST"))
            resp = db.Locate(self._name)
            host, port = resp.host, resp.port
            self.__server_address = f"{host}:{port}"
            # reply = db.import_device(self._name)
            # self.__server_address = reply.info.address
        return self.__server_address

    @property
    def _grpc_client(self):
        if not self.__grpc_client:
            self.__grpc_client = GrpcClient(self._server_address)
        return self.__grpc_client

    def add_logging_target(self, *args, **kwargs):
        raise NotImplementedError()

    def adm_name(self, *args, **kwargs):
        raise NotImplementedError()

    def alias(self, *args, **kwargs):
        # TODO: Needed for  webjive
        raise DevFailed
        raise NotImplementedError()

    def attribute_history(self, *args, **kwargs):
        raise NotImplementedError()

    def attribute_list_query(self, *args, **kwargs):
        # TODO: it is suppost to be an attribute info, not a attribute info ex
        output = []
        for attr in self.get_attribute_list():
            output.append(self.get_attribute_config(attr))
        return output

    def attribute_list_query_ex(self, *args, **kwargs):
        raise NotImplementedError()

    def attribute_query(self, attribute, *args, **kwargs):
        return self.get_attribute_config(attribute)

    def black_box(self, *args, **kwargs):
        raise NotImplementedError()

    def cancel_all_polling_asynch_request(self, *args, **kwargs):
        raise NotImplementedError()

    def cancel_asynch_request(self, *args, **kwargs):
        raise NotImplementedError()

    def command_history(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout(self, cmd, *args, **kwargs):
        rep = self._grpc_client.command_inout(self._name, cmd)
        if not rep.success:
            raise DevFailed(rep.error.message)

    def command_inout_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout_raw(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def command_inout_reply_raw(self, *args, **kwargs):
        raise NotImplementedError()

    def command_list_query(self, *args, **kwargs):
        # Shall return a list of CommandInfo
        # TODO: Needed for  webjive
        return []
        raise NotImplementedError()

    def command_query(self, *args, **kwargs):
        raise NotImplementedError()

    def connect(self, *args, **kwargs):
        raise NotImplementedError()

    def delete_property(self, *args, **kwargs):
        raise NotImplementedError()

    def description(self, *args, **kwargs):
        raise NotImplementedError()

    def dev_name(self, *args, **kwargs):
        raise NotImplementedError()

    def event_queue_size(self, *args, **kwargs):
        raise NotImplementedError()

    def get_access_control(self, *args, **kwargs):
        raise NotImplementedError()

    def get_access_right(self, *args, **kwargs):
        raise NotImplementedError()

    def get_asynch_replies(self, *args, **kwargs):
        raise NotImplementedError()

    def get_attribute_config(self, attr_name, *args, **kwargs):
        attr = self._grpc_client.read_attribute_config(self._name, attr_name)
        if not attr.success:
            raise DevFailed(attr.error.message)
        return build_attribute_info_ex(attr.data)

    def get_attribute_config_ex(self, *args, **kwargs):
        raise NotImplementedError()

    def get_attribute_list(self):
        attr_list = self._grpc_client.get_attribute_list(self._name)
        return [attr.name for attr in attr_list]

    def get_attribute_poll_period(self, *args, **kwargs):
        raise NotImplementedError()

    def get_command_config(self, *args, **kwargs):
        raise NotImplementedError()

    def get_command_list(self, *args, **kwargs):
        raise NotImplementedError()

    def get_command_poll_period(self, *args, **kwargs):
        raise NotImplementedError()

    def get_db_host(self, *args, **kwargs):
        raise NotImplementedError()

    def get_db_port(self, *args, **kwargs):
        raise NotImplementedError()

    def get_db_port_num(self, *args, **kwargs):
        raise NotImplementedError()

    def get_dev_host(self, *args, **kwargs):
        raise NotImplementedError()

    def get_dev_port(self, *args, **kwargs):
        raise NotImplementedError()

    def get_device_db(self, *args, **kwargs):
        raise NotImplementedError()

    def get_events(self, *args, **kwargs):
        raise NotImplementedError()

    def get_fqdn(self, *args, **kwargs):
        raise NotImplementedError()

    def get_from_env_var(self, *args, **kwargs):
        raise NotImplementedError()

    def get_idl_version(self, *args, **kwargs):
        raise NotImplementedError()

    def get_last_event_date(self, *args, **kwargs):
        raise NotImplementedError()

    def get_locker(self, *args, **kwargs):
        raise NotImplementedError()

    def get_logging_level(self, *args, **kwargs):
        raise NotImplementedError()

    def get_logging_target(self, *args, **kwargs):
        raise NotImplementedError()

    def get_pipe_config(self, *args, **kwargs):
        raise NotImplementedError()

    def get_pipe_list(self, *args, **kwargs):
        raise NotImplementedError()

    def get_property(self, *args, **kwargs):
        raise NotImplementedError()

    def get_property_list(self, *args, **kwargs):
        raise NotImplementedError()

    def get_source(self, *args, **kwargs):
        raise NotImplementedError()

    def get_tango_lib_version(self, *args, **kwargs):
        raise NotImplementedError()

    def get_timeout_millis(self, *args, **kwargs):
        raise NotImplementedError()

    def get_transparency_reconnection(self, *args, **kwargs):
        raise NotImplementedError()

    def import_info(self, *args, **kwargs):
        raise NotImplementedError()

    def info(self, *args, **kwargs):
        # TODO: Needed for  webjive
        # TODO: feed it
        return DeviceInfo()

    def init(self, *args, **kwargs):
        raise NotImplementedError()

    def is_attribute_polled(self, *args, **kwargs):
        raise NotImplementedError()

    def is_command_polled(self, *args, **kwargs):
        raise NotImplementedError()

    def is_dbase_used(self, *args, **kwargs):
        raise NotImplementedError()

    def is_event_queue_empty(self, *args, **kwargs):
        raise NotImplementedError()

    def is_locked(self, *args, **kwargs):
        raise NotImplementedError()

    def is_locked_by_me(self, *args, **kwargs):
        raise NotImplementedError()

    def lock(self, *args, **kwargs):
        raise NotImplementedError()

    def locking_status(self, *args, **kwargs):
        raise NotImplementedError()

    def name(self, *args, **kwargs):
        raise NotImplementedError()

    def pending_asynch_call(self, *args, **kwargs):
        raise NotImplementedError()

    def ping(self, *args, **kwargs):
        raise NotImplementedError()

    def poll_attribute(self, *args, **kwargs):
        raise NotImplementedError()

    def poll_command(self, *args, **kwargs):
        raise NotImplementedError()

    def polling_status(self, *args, **kwargs):
        raise NotImplementedError()

    def put_property(self, *args, **kwargs):
        raise NotImplementedError()

    def _read_attribute(self, attr_name, *args, **kwargs):
        attr = self._grpc_client.read_attribute(self._name, attr_name)
        if not attr.success:
            raise DevFailed(attr.error.message)
        return DeviceAttribute.build_from_grpc(attr.data)

    def read_attribute_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attribute_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attributes(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attributes_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def read_attributes_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def read_pipe(self, *args, **kwargs):
        raise NotImplementedError()

    def reconnect(self, *args, **kwargs):
        raise NotImplementedError()

    def remove_logging_target(self, *args, **kwargs):
        raise NotImplementedError()

    def set_access_control(self, *args, **kwargs):
        raise NotImplementedError()

    def set_attribute_config(self, *args, **kwargs):
        raise NotImplementedError()

    def set_logging_level(self, *args, **kwargs):
        raise NotImplementedError()

    def set_pipe_config(self, *args, **kwargs):
        raise NotImplementedError()

    def set_source(self, *args, **kwargs):
        raise NotImplementedError()

    def set_timeout_millis(self, *args, **kwargs):
        raise NotImplementedError()

    def set_transparency_reconnection(self, *args, **kwargs):
        raise NotImplementedError()

    def State(self, *args, **kwargs):
        return self._grpc_client.read_attribute(self._name, "state").value

    def Status(self, *args, **kwargs):
        return self._grpc_client.read_attribute(self._name, "status").value

    def stop_poll_attribute(self, *args, **kwargs):
        raise NotImplementedError()

    def stop_poll_command(self, *args, **kwargs):
        raise NotImplementedError()

    def subscribe_event(self, *args, **kwargs):
        raise NotImplementedError()

    def unlock(self, *args, **kwargs):
        raise NotImplementedError()

    def unsubscribe_event(self, *args, **kwargs):
        raise NotImplementedError()

    def _write_attribute(self, attr_name, value, *args, **kwargs):
        attr = self._grpc_client.write_attribute(self._name, attr_name, value)
        if not attr.success:
            raise DevFailed(attr.error.message)

    def write_attribute_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attribute_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attributes(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attributes_asynch(self, *args, **kwargs):
        raise NotImplementedError()

    def write_attributes_reply(self, *args, **kwargs):
        raise NotImplementedError()

    def write_pipe(self, *args, **kwargs):
        raise NotImplementedError()

    def write_read_attribute(self, name, value):
        self.write_attribute(name, value)
        return self.read_attribute(name)

    def write_read_attributes(self, *args, **kwargs):
        raise NotImplementedError()


DeviceProxy.read_attribute = green(DeviceProxy._read_attribute)
DeviceProxy.write_attribute = green(DeviceProxy._write_attribute)
DeviceProxy.state = green(DeviceProxy._state)


if __name__ == "__main__":
    ds = DeviceProxy("sys/tg_test/1")
    print(ds.read_attribute("ampli"))
    print(ds.read_attribute("bool_attr"))
    print(ds.read_attribute("float_attr"))
    print(ds.read_attribute("string_attr"))
    print("Read attribute configuration")
    print(ds.get_attribute_config("ampli"))
    print("Read attribute error")
    print(ds.read_attribute("error"))
