import logging

# Others
import traceback

from mascot.grpcproto.attribute_config_pb2 import AttributeConfigReply
from mascot.grpcproto.attributes_pb2 import ReadAttributeReply, WriteReply
from mascot.grpcproto.commands_pb2 import CommandReply
from mascot.grpcproto.device_server_pb2_grpc import MascotDeviceServerServicer
from mascot.grpcproto.error_pb2 import Error
from mascot.grpcproto.device_pb2 import AttributeListReply, DeviceAttributeDescription

# local imports
from mascot.utils import value_unpacker

# Grpc and run server

# TODO: logging Shall be moved
logging.basicConfig(level=logging.DEBUG)


class DeviceServicer(MascotDeviceServerServicer):
    """ The DeviceServer grpc service used to handle rpc request"""

    def __init__(self, servername):
        """ Setup a new Mascot server """
        self.servername = servername
        self.logger = logging.getLogger(servername)
        # Locals
        self.device_list = {}

    def add_device(self, name, device):
        """ Register a new device instance served by this server """
        self.logger.info("Register device {} : {}".format(device, name))
        self.device_list[name] = device

    def ReadAttribute(self, request, context):
        """ Perform a ReadAttribute grpc request """
        log = "Get read_attribute, request {} {}"
        self.logger.debug(log.format(request, context))
        # Unpack grpc request
        device_name = request.deviceName
        attribute = request.attributeName
        # Get the associated object.
        # TODO device not in the list
        device = self.device_list[device_name]
        try:
            log = "Read request on device: {}/ attr: {}"
            self.logger.debug(log.format(device, attribute))
            # Perform a read request
            attr = device.read_attribute(attribute)
        except Exception:
            # Something wrong happen, logit
            self.logger.error(traceback.format_exc())
            # Build an Error answer
            error = Error(code=-1, message=traceback.format_exc())
            return ReadAttributeReply(success=False, error=error)
        log = "Read request succeed on device: {}. attr: {}"
        self.logger.debug(log.format(device, attribute))
        return ReadAttributeReply(success=True, data=attr)

    def ReadAttributeConfig(self, request, context):
        """ Perform a ReadAttributeConfig grpc request """
        log = "Get read_attribute, request {} {}"
        self.logger.debug(log.format(request, context))
        # Unpack grpc request
        device_name = request.deviceName
        attribute = request.attributeName
        # Get the associated object.
        # TODO device not in the list
        device = self.device_list[device_name]
        try:
            # Perform a read request
            attr = device.read_attribute_config(attribute)
        except Exception:
            # Something wrong happen, logit
            self.logger.error(traceback.format_exc())
            # Build an Error answer
            error = Error(code=-1, message=traceback.format_exc())
            return AttributeConfigReply(success=False, error=error)
        return AttributeConfigReply(success=True, data=attr)

    def WriteAttribute(self, request, context):
        log = "Get write_attribute, request {} {}"
        self.logger.debug(log.format(request, context))
        # Unpack grpc request
        device_name = request.deviceName
        attribute = request.attributeName
        value_type, value = value_unpacker(request.value)
        device = self.device_list[device_name]
        try:
            log = "Write request on device: {}/ attr: {} / value: {}"
            self.logger.debug(log.format(device, attribute, value))
            # Perform a write request
            device.write_attribute(attribute, value)
        except Exception:
            # Something wrong happen, logit
            self.logger.error(traceback.format_exc())
            # Build an Error answer
            error = Error(code=-1, message=traceback.format_exc())
            return WriteReply(success=False, error=error)
        log = "Write request succeed on device: {}/ attr: {} / value: {}"
        self.logger.debug(log.format(device, attribute, value))
        return WriteReply(success=True)

    def ExecuteCommand(self, request, context):
        log = "ExecuteCommand, request {} {}"
        self.logger.debug(log.format(request, context))
        device_name = request.deviceName
        command_name = request.commandName
        print(command_name)
        kwargs = request.parameters
        device = self.device_list[device_name]
        try:

            cmd_out = device.exec_command(command_name, kwargs)
            if cmd_out is None:
                cmd_out = {}
        except Exception:
            # Something wrong happen, logit
            self.logger.error(traceback.format_exc())
            # Build an Error answer
            error = Error(code=-1, message=traceback.format_exc())
            return CommandReply(success=False, error=error)
        return CommandReply(success=True, values=cmd_out)

    # GetAttributeList(GetAttributeListMessage) returns (AttributeListReply) {}
    def GetAttributeList(self, request, context):
        log = f"Get attribute list, {request}"
        self.logger.debug(log.format(request, context))
        device_name = request.deviceName
        device = self.device_list[device_name]
        attr_list = []
        for attr_name, attr in device._attr_map.items():
            # TODO set writable
            attr_list.append(
                DeviceAttributeDescription(
                    name=attr_name, dataType=attr.get_data_type().value, writable=True
                )
            )
        reply = AttributeListReply()
        reply.attributes.extend(attr_list)
        return reply
