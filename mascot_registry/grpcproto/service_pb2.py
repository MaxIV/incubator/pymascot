# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: service.proto

import sys

_b = sys.version_info[0] < 3 and (lambda x: x) or (lambda x: x.encode("latin1"))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database

# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor.FileDescriptor(
    name="service.proto",
    package="mascot_registry_pb",
    syntax="proto3",
    serialized_options=None,
    serialized_pb=_b(
        '\n\rservice.proto\x12\x12mascot_registry_pb"\x07\n\x05\x45mpty"N\n\nErrorReply\x12\x0f\n\x07success\x18\x01 \x01(\x08\x12\x14\n\x0c\x65rror_number\x18\x02 \x01(\x03\x12\x19\n\x11\x65rror_description\x18\x03 \x01(\t"K\n\x06\x44\x65vice\x12\x13\n\x0bmascotclass\x18\x01 \x01(\t\x12,\n\x04name\x18\x02 \x01(\x0b\x32\x1e.mascot_registry_pb.DeviceName"@\n\nDeviceName\x12\x0e\n\x06\x66\x61mily\x18\x01 \x01(\t\x12\x0e\n\x06\x64omain\x18\x02 \x01(\t\x12\x12\n\nidentifier\x18\x03 \x01(\t"W\n\x13\x43reateDeviceMessage\x12+\n\x07\x64\x65vices\x18\x01 \x03(\x0b\x32\x1a.mascot_registry_pb.Device\x12\x13\n\x0bserver_name\x18\x02 \x01(\t"F\n\x13\x44\x65leteDeviceMessage\x12/\n\x07\x64\x65vices\x18\x01 \x03(\x0b\x32\x1e.mascot_registry_pb.DeviceName"G\n\x14PublishServerMessage\x12\x13\n\x0bserver_name\x18\x01 \x01(\t\x12\x0c\n\x04host\x18\x02 \x01(\t\x12\x0c\n\x04port\x18\x03 \x01(\x03"-\n\x16UnpublishServerMessage\x12\x13\n\x0bserver_name\x18\x01 \x01(\t")\n\x12ListDevicesMessage\x12\x13\n\x0bserver_name\x18\x01 \x01(\t"V\n\x10ListServersReply\x12-\n\x05\x65rror\x18\x01 \x01(\x0b\x32\x1e.mascot_registry_pb.ErrorReply\x12\x13\n\x0bserver_name\x18\x02 \x03(\t"n\n\x10ListDevicesReply\x12-\n\x05\x65rror\x18\x01 \x01(\x0b\x32\x1e.mascot_registry_pb.ErrorReply\x12+\n\x07\x64\x65vices\x18\x02 \x03(\x0b\x32\x1a.mascot_registry_pb.Device"m\n\x0bLocateReply\x12-\n\x05\x65rror\x18\x01 \x01(\x0b\x32\x1e.mascot_registry_pb.ErrorReply\x12\x13\n\x0bserver_name\x18\x02 \x01(\t\x12\x0c\n\x04host\x18\x03 \x01(\t\x12\x0c\n\x04port\x18\x04 \x01(\x03\x32\x92\x06\n\x08Registry\x12W\n\x0c\x43reateDevice\x12\'.mascot_registry_pb.CreateDeviceMessage\x1a\x1e.mascot_registry_pb.ErrorReply\x12W\n\x0c\x44\x65leteDevice\x12\'.mascot_registry_pb.DeleteDeviceMessage\x1a\x1e.mascot_registry_pb.ErrorReply\x12Y\n\rPublishServer\x12(.mascot_registry_pb.PublishServerMessage\x1a\x1e.mascot_registry_pb.ErrorReply\x12]\n\x0fUnpublishServer\x12*.mascot_registry_pb.UnpublishServerMessage\x1a\x1e.mascot_registry_pb.ErrorReply\x12N\n\x0bListServers\x12\x19.mascot_registry_pb.Empty\x1a$.mascot_registry_pb.ListServersReply\x12[\n\x0bListDevices\x12&.mascot_registry_pb.ListDevicesMessage\x1a$.mascot_registry_pb.ListDevicesReply\x12O\n\rPublishDevice\x12\x1e.mascot_registry_pb.DeviceName\x1a\x1e.mascot_registry_pb.ErrorReply\x12Q\n\x0fUnpublishDevice\x12\x1e.mascot_registry_pb.DeviceName\x1a\x1e.mascot_registry_pb.ErrorReply\x12I\n\x06Locate\x12\x1e.mascot_registry_pb.DeviceName\x1a\x1f.mascot_registry_pb.LocateReplyb\x06proto3'
    ),
)


_EMPTY = _descriptor.Descriptor(
    name="Empty",
    full_name="mascot_registry_pb.Empty",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=37,
    serialized_end=44,
)


_ERRORREPLY = _descriptor.Descriptor(
    name="ErrorReply",
    full_name="mascot_registry_pb.ErrorReply",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="success",
            full_name="mascot_registry_pb.ErrorReply.success",
            index=0,
            number=1,
            type=8,
            cpp_type=7,
            label=1,
            has_default_value=False,
            default_value=False,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="error_number",
            full_name="mascot_registry_pb.ErrorReply.error_number",
            index=1,
            number=2,
            type=3,
            cpp_type=2,
            label=1,
            has_default_value=False,
            default_value=0,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="error_description",
            full_name="mascot_registry_pb.ErrorReply.error_description",
            index=2,
            number=3,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=46,
    serialized_end=124,
)


_DEVICE = _descriptor.Descriptor(
    name="Device",
    full_name="mascot_registry_pb.Device",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="mascotclass",
            full_name="mascot_registry_pb.Device.mascotclass",
            index=0,
            number=1,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="name",
            full_name="mascot_registry_pb.Device.name",
            index=1,
            number=2,
            type=11,
            cpp_type=10,
            label=1,
            has_default_value=False,
            default_value=None,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=126,
    serialized_end=201,
)


_DEVICENAME = _descriptor.Descriptor(
    name="DeviceName",
    full_name="mascot_registry_pb.DeviceName",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="family",
            full_name="mascot_registry_pb.DeviceName.family",
            index=0,
            number=1,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="domain",
            full_name="mascot_registry_pb.DeviceName.domain",
            index=1,
            number=2,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="identifier",
            full_name="mascot_registry_pb.DeviceName.identifier",
            index=2,
            number=3,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=203,
    serialized_end=267,
)


_CREATEDEVICEMESSAGE = _descriptor.Descriptor(
    name="CreateDeviceMessage",
    full_name="mascot_registry_pb.CreateDeviceMessage",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="devices",
            full_name="mascot_registry_pb.CreateDeviceMessage.devices",
            index=0,
            number=1,
            type=11,
            cpp_type=10,
            label=3,
            has_default_value=False,
            default_value=[],
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="server_name",
            full_name="mascot_registry_pb.CreateDeviceMessage.server_name",
            index=1,
            number=2,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=269,
    serialized_end=356,
)


_DELETEDEVICEMESSAGE = _descriptor.Descriptor(
    name="DeleteDeviceMessage",
    full_name="mascot_registry_pb.DeleteDeviceMessage",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="devices",
            full_name="mascot_registry_pb.DeleteDeviceMessage.devices",
            index=0,
            number=1,
            type=11,
            cpp_type=10,
            label=3,
            has_default_value=False,
            default_value=[],
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        )
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=358,
    serialized_end=428,
)


_PUBLISHSERVERMESSAGE = _descriptor.Descriptor(
    name="PublishServerMessage",
    full_name="mascot_registry_pb.PublishServerMessage",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="server_name",
            full_name="mascot_registry_pb.PublishServerMessage.server_name",
            index=0,
            number=1,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="host",
            full_name="mascot_registry_pb.PublishServerMessage.host",
            index=1,
            number=2,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="port",
            full_name="mascot_registry_pb.PublishServerMessage.port",
            index=2,
            number=3,
            type=3,
            cpp_type=2,
            label=1,
            has_default_value=False,
            default_value=0,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=430,
    serialized_end=501,
)


_UNPUBLISHSERVERMESSAGE = _descriptor.Descriptor(
    name="UnpublishServerMessage",
    full_name="mascot_registry_pb.UnpublishServerMessage",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="server_name",
            full_name="mascot_registry_pb.UnpublishServerMessage.server_name",
            index=0,
            number=1,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        )
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=503,
    serialized_end=548,
)


_LISTDEVICESMESSAGE = _descriptor.Descriptor(
    name="ListDevicesMessage",
    full_name="mascot_registry_pb.ListDevicesMessage",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="server_name",
            full_name="mascot_registry_pb.ListDevicesMessage.server_name",
            index=0,
            number=1,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        )
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=550,
    serialized_end=591,
)


_LISTSERVERSREPLY = _descriptor.Descriptor(
    name="ListServersReply",
    full_name="mascot_registry_pb.ListServersReply",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="error",
            full_name="mascot_registry_pb.ListServersReply.error",
            index=0,
            number=1,
            type=11,
            cpp_type=10,
            label=1,
            has_default_value=False,
            default_value=None,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="server_name",
            full_name="mascot_registry_pb.ListServersReply.server_name",
            index=1,
            number=2,
            type=9,
            cpp_type=9,
            label=3,
            has_default_value=False,
            default_value=[],
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=593,
    serialized_end=679,
)


_LISTDEVICESREPLY = _descriptor.Descriptor(
    name="ListDevicesReply",
    full_name="mascot_registry_pb.ListDevicesReply",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="error",
            full_name="mascot_registry_pb.ListDevicesReply.error",
            index=0,
            number=1,
            type=11,
            cpp_type=10,
            label=1,
            has_default_value=False,
            default_value=None,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="devices",
            full_name="mascot_registry_pb.ListDevicesReply.devices",
            index=1,
            number=2,
            type=11,
            cpp_type=10,
            label=3,
            has_default_value=False,
            default_value=[],
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=681,
    serialized_end=791,
)


_LOCATEREPLY = _descriptor.Descriptor(
    name="LocateReply",
    full_name="mascot_registry_pb.LocateReply",
    filename=None,
    file=DESCRIPTOR,
    containing_type=None,
    fields=[
        _descriptor.FieldDescriptor(
            name="error",
            full_name="mascot_registry_pb.LocateReply.error",
            index=0,
            number=1,
            type=11,
            cpp_type=10,
            label=1,
            has_default_value=False,
            default_value=None,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="server_name",
            full_name="mascot_registry_pb.LocateReply.server_name",
            index=1,
            number=2,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="host",
            full_name="mascot_registry_pb.LocateReply.host",
            index=2,
            number=3,
            type=9,
            cpp_type=9,
            label=1,
            has_default_value=False,
            default_value=_b("").decode("utf-8"),
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
        _descriptor.FieldDescriptor(
            name="port",
            full_name="mascot_registry_pb.LocateReply.port",
            index=3,
            number=4,
            type=3,
            cpp_type=2,
            label=1,
            has_default_value=False,
            default_value=0,
            message_type=None,
            enum_type=None,
            containing_type=None,
            is_extension=False,
            extension_scope=None,
            serialized_options=None,
            file=DESCRIPTOR,
        ),
    ],
    extensions=[],
    nested_types=[],
    enum_types=[],
    serialized_options=None,
    is_extendable=False,
    syntax="proto3",
    extension_ranges=[],
    oneofs=[],
    serialized_start=793,
    serialized_end=902,
)

_DEVICE.fields_by_name["name"].message_type = _DEVICENAME
_CREATEDEVICEMESSAGE.fields_by_name["devices"].message_type = _DEVICE
_DELETEDEVICEMESSAGE.fields_by_name["devices"].message_type = _DEVICENAME
_LISTSERVERSREPLY.fields_by_name["error"].message_type = _ERRORREPLY
_LISTDEVICESREPLY.fields_by_name["error"].message_type = _ERRORREPLY
_LISTDEVICESREPLY.fields_by_name["devices"].message_type = _DEVICE
_LOCATEREPLY.fields_by_name["error"].message_type = _ERRORREPLY
DESCRIPTOR.message_types_by_name["Empty"] = _EMPTY
DESCRIPTOR.message_types_by_name["ErrorReply"] = _ERRORREPLY
DESCRIPTOR.message_types_by_name["Device"] = _DEVICE
DESCRIPTOR.message_types_by_name["DeviceName"] = _DEVICENAME
DESCRIPTOR.message_types_by_name["CreateDeviceMessage"] = _CREATEDEVICEMESSAGE
DESCRIPTOR.message_types_by_name["DeleteDeviceMessage"] = _DELETEDEVICEMESSAGE
DESCRIPTOR.message_types_by_name["PublishServerMessage"] = _PUBLISHSERVERMESSAGE
DESCRIPTOR.message_types_by_name["UnpublishServerMessage"] = _UNPUBLISHSERVERMESSAGE
DESCRIPTOR.message_types_by_name["ListDevicesMessage"] = _LISTDEVICESMESSAGE
DESCRIPTOR.message_types_by_name["ListServersReply"] = _LISTSERVERSREPLY
DESCRIPTOR.message_types_by_name["ListDevicesReply"] = _LISTDEVICESREPLY
DESCRIPTOR.message_types_by_name["LocateReply"] = _LOCATEREPLY
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Empty = _reflection.GeneratedProtocolMessageType(
    "Empty",
    (_message.Message,),
    dict(
        DESCRIPTOR=_EMPTY,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.Empty)
    ),
)
_sym_db.RegisterMessage(Empty)

ErrorReply = _reflection.GeneratedProtocolMessageType(
    "ErrorReply",
    (_message.Message,),
    dict(
        DESCRIPTOR=_ERRORREPLY,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.ErrorReply)
    ),
)
_sym_db.RegisterMessage(ErrorReply)

Device = _reflection.GeneratedProtocolMessageType(
    "Device",
    (_message.Message,),
    dict(
        DESCRIPTOR=_DEVICE,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.Device)
    ),
)
_sym_db.RegisterMessage(Device)

DeviceName = _reflection.GeneratedProtocolMessageType(
    "DeviceName",
    (_message.Message,),
    dict(
        DESCRIPTOR=_DEVICENAME,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.DeviceName)
    ),
)
_sym_db.RegisterMessage(DeviceName)

CreateDeviceMessage = _reflection.GeneratedProtocolMessageType(
    "CreateDeviceMessage",
    (_message.Message,),
    dict(
        DESCRIPTOR=_CREATEDEVICEMESSAGE,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.CreateDeviceMessage)
    ),
)
_sym_db.RegisterMessage(CreateDeviceMessage)

DeleteDeviceMessage = _reflection.GeneratedProtocolMessageType(
    "DeleteDeviceMessage",
    (_message.Message,),
    dict(
        DESCRIPTOR=_DELETEDEVICEMESSAGE,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.DeleteDeviceMessage)
    ),
)
_sym_db.RegisterMessage(DeleteDeviceMessage)

PublishServerMessage = _reflection.GeneratedProtocolMessageType(
    "PublishServerMessage",
    (_message.Message,),
    dict(
        DESCRIPTOR=_PUBLISHSERVERMESSAGE,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.PublishServerMessage)
    ),
)
_sym_db.RegisterMessage(PublishServerMessage)

UnpublishServerMessage = _reflection.GeneratedProtocolMessageType(
    "UnpublishServerMessage",
    (_message.Message,),
    dict(
        DESCRIPTOR=_UNPUBLISHSERVERMESSAGE,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.UnpublishServerMessage)
    ),
)
_sym_db.RegisterMessage(UnpublishServerMessage)

ListDevicesMessage = _reflection.GeneratedProtocolMessageType(
    "ListDevicesMessage",
    (_message.Message,),
    dict(
        DESCRIPTOR=_LISTDEVICESMESSAGE,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.ListDevicesMessage)
    ),
)
_sym_db.RegisterMessage(ListDevicesMessage)

ListServersReply = _reflection.GeneratedProtocolMessageType(
    "ListServersReply",
    (_message.Message,),
    dict(
        DESCRIPTOR=_LISTSERVERSREPLY,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.ListServersReply)
    ),
)
_sym_db.RegisterMessage(ListServersReply)

ListDevicesReply = _reflection.GeneratedProtocolMessageType(
    "ListDevicesReply",
    (_message.Message,),
    dict(
        DESCRIPTOR=_LISTDEVICESREPLY,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.ListDevicesReply)
    ),
)
_sym_db.RegisterMessage(ListDevicesReply)

LocateReply = _reflection.GeneratedProtocolMessageType(
    "LocateReply",
    (_message.Message,),
    dict(
        DESCRIPTOR=_LOCATEREPLY,
        __module__="service_pb2"
        # @@protoc_insertion_point(class_scope:mascot_registry_pb.LocateReply)
    ),
)
_sym_db.RegisterMessage(LocateReply)


_REGISTRY = _descriptor.ServiceDescriptor(
    name="Registry",
    full_name="mascot_registry_pb.Registry",
    file=DESCRIPTOR,
    index=0,
    serialized_options=None,
    serialized_start=905,
    serialized_end=1691,
    methods=[
        _descriptor.MethodDescriptor(
            name="CreateDevice",
            full_name="mascot_registry_pb.Registry.CreateDevice",
            index=0,
            containing_service=None,
            input_type=_CREATEDEVICEMESSAGE,
            output_type=_ERRORREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="DeleteDevice",
            full_name="mascot_registry_pb.Registry.DeleteDevice",
            index=1,
            containing_service=None,
            input_type=_DELETEDEVICEMESSAGE,
            output_type=_ERRORREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="PublishServer",
            full_name="mascot_registry_pb.Registry.PublishServer",
            index=2,
            containing_service=None,
            input_type=_PUBLISHSERVERMESSAGE,
            output_type=_ERRORREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="UnpublishServer",
            full_name="mascot_registry_pb.Registry.UnpublishServer",
            index=3,
            containing_service=None,
            input_type=_UNPUBLISHSERVERMESSAGE,
            output_type=_ERRORREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="ListServers",
            full_name="mascot_registry_pb.Registry.ListServers",
            index=4,
            containing_service=None,
            input_type=_EMPTY,
            output_type=_LISTSERVERSREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="ListDevices",
            full_name="mascot_registry_pb.Registry.ListDevices",
            index=5,
            containing_service=None,
            input_type=_LISTDEVICESMESSAGE,
            output_type=_LISTDEVICESREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="PublishDevice",
            full_name="mascot_registry_pb.Registry.PublishDevice",
            index=6,
            containing_service=None,
            input_type=_DEVICENAME,
            output_type=_ERRORREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="UnpublishDevice",
            full_name="mascot_registry_pb.Registry.UnpublishDevice",
            index=7,
            containing_service=None,
            input_type=_DEVICENAME,
            output_type=_ERRORREPLY,
            serialized_options=None,
        ),
        _descriptor.MethodDescriptor(
            name="Locate",
            full_name="mascot_registry_pb.Registry.Locate",
            index=8,
            containing_service=None,
            input_type=_DEVICENAME,
            output_type=_LOCATEREPLY,
            serialized_options=None,
        ),
    ],
)
_sym_db.RegisterServiceDescriptor(_REGISTRY)

DESCRIPTOR.services_by_name["Registry"] = _REGISTRY

# @@protoc_insertion_point(module_scope)
