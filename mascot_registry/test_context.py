from concurrent import futures
from contextlib import contextmanager

import grpc

from mascot_registry.jsonregistry_server import ServerRegistry
from mascot_registry.client_registry import ClientRegistry
from mascot_registry.grpcproto.service_pb2_grpc import add_RegistryServicer_to_server

__all__ = "RegistryTestContext"


@contextmanager
def RegistryTestContext(host="0.0.0.0", port=50000):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    directory = ServerRegistry()
    add_RegistryServicer_to_server(directory, server)
    host = f"{host}:{port}"
    server.add_insecure_port(host)
    server.start()
    yield directory, ClientRegistry(database_host=host)
    server.stop(0)
