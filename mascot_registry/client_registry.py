import os
import grpc
from mascot_registry.grpcproto.service_pb2_grpc import RegistryStub
from mascot_registry.grpcproto.service_pb2 import (
    CreateDeviceMessage,
    DeleteDeviceMessage,
)
from mascot_registry.grpcproto.service_pb2 import Device, DeviceName
from mascot_registry.grpcproto.service_pb2 import (
    PublishServerMessage,
    UnpublishServerMessage,
)
from mascot_registry.grpcproto.service_pb2 import ListDevicesMessage
from mascot_registry.grpcproto.service_pb2 import ListDevicesReply
from mascot_registry.grpcproto.service_pb2 import ErrorReply
from mascot_registry.grpcproto.service_pb2 import LocateReply
from mascot_registry.grpcproto.service_pb2 import Empty

# TODO lower functions
class ClientRegistry:
    def __init__(self, database_host=None):
        if not database_host:
            database_host = os.getenv("TANGO_HOST")
        self.host = database_host
        self.channel = grpc.insecure_channel(self.host)
        self.directory = RegistryStub(self.channel)

    def CreateDevice(self, device: str, mascotclass: str, server: str):
        domain, family, member = device.split("/")
        devname = DeviceName(domain=domain, family=family, identifier=member)
        device = Device(mascotclass=mascotclass, name=devname)
        msg = CreateDeviceMessage(server_name=server)
        msg.devices.extend([device])
        # TODO: RaiseError
        return self.directory.CreateDevice(msg)

    def DeleteDevice(self, device: str):
        domain, family, member = device.split("/")
        devname = DeviceName(domain=domain, family=family, identifier=member)
        msg = DeleteDeviceMessage()
        msg.devices.extend([devname])
        return self.directory.DeleteDevice(msg)

    def PublishServer(self, srv_name: str, host: str, port: str):
        msg = PublishServerMessage(server_name=srv_name, host=host, port=port)
        return self.directory.PublishServer(msg)

    def UnpublishServer(self, srv_name: str):
        msg = UnpublishServerMessage(server_name=srv_name)
        return self.directory.UnpublishServer(msg)

    def ListServers(self):
        rep = self.directory.ListServers(Empty())
        return rep

    def ListDevices(self, srv_name: str):
        msg = ListDevicesMessage(server_name=srv_name)
        reply = self.directory.ListDevices(msg)
        return reply

    def PublishDevice(self, device: str):
        domain, family, member = device.split("/")
        msg = DeviceName(domain=domain, family=family, identifier=member)
        ret = self.directory.PublishDevice(msg)
        return ret

    def UnpublishDevice(self, device: str):
        domain, family, member = device.split("/")
        msg = DeviceName(domain=domain, family=family, identifier=member)
        ret = self.directory.UnpublishDevice(msg)
        return ret

    def Locate(self, device: str):
        domain, family, member = device.split("/")
        devname = DeviceName(domain=domain, family=family, identifier=member)
        reply = self.directory.Locate(devname)
        return reply
