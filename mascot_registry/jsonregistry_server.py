import grpc
from time import sleep

from concurrent import futures

from mascot_registry.grpcproto.service_pb2 import ErrorReply
from mascot_registry.grpcproto.service_pb2 import ListServersReply
from mascot_registry.grpcproto.service_pb2 import ListDevicesReply
from mascot_registry.grpcproto.service_pb2 import LocateReply
from mascot_registry.grpcproto.service_pb2 import Device, DeviceName
from mascot_registry.grpcproto.service_pb2_grpc import add_RegistryServicer_to_server
from mascot_registry.grpcproto.service_pb2_grpc import RegistryServicer

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
import traceback
import json
import logging
import argparse


def build_device_name(domain, family, member):
    return "/".join((domain, family, member))


@dataclass_json
@dataclass
class DeviceData:
    name: str
    domain: str
    family: str
    member: str
    mascotclass: str
    exported: bool = False


@dataclass_json
@dataclass
class ServerData:
    server_name: str
    host: str = ""
    port: int = 0
    devices: [DeviceData] = field(default_factory=list)

    def build_dict(self):
        devices = [device.to_dict() for device in self.devices]
        dct = {
            "server_name": self.server_name,
            "host": self.host,
            "port": self.port,
            "devices": devices,
        }
        return dct

    def __contains__(self, value):
        return value in [dev.name for dev in self.devices]

    def __getitem__(self, item):
        if item not in self:
            err = f"Device not found {item} in server {self.server_name}"
            raise KeyError(err)
        for dev in self.devices:
            if dev.name == item:
                return dev

    def new_device(self, domain, family, member, device_class, name=""):
        if not name:
            name = build_device_name(domain, family, member)
        if name not in self:
            devdata = DeviceData(
                name=name,
                domain=domain,
                family=family,
                member=member,
                mascotclass=device_class,
            )
            self.devices.append(devdata)
            return devdata
        raise ValueError(f"Device {name} already exist")

    def del_device(self, domain, family, member):
        name = build_device_name(domain, family, member)
        for dev in self.devices:
            if dev.name == name:
                self.devices.remove(dev)
                return
        raise ValueError(f"Device {name} does not exist")


@dataclass_json
@dataclass
class Registry:
    servers: [ServerData] = field(default_factory=list)

    def build_dict(self):
        return {"servers": [server.build_dict() for server in self.servers]}

    def __iter__(self):
        return self.servers.__iter__()

    def __contains__(self, value):
        return value in [srv.server_name for srv in self.servers]

    def __getitem__(self, item):
        if item not in self:
            err = f"Server {item} not found"
            raise KeyError(err)
        for srv in self.servers:
            if srv.server_name == item:
                return srv

    def new_server(self, server_name):
        srv = ServerData(server_name=server_name)
        self.servers.append(srv)
        return srv

    def del_server(self, server_name):
        for srv in self.servers:
            if srv.server_name == server_name:
                self.servers.remove(srv)
                return
        raise ValueError(f"Server {server_name} does not exist")

    def new_device(self, server_name, domain, family, member, device_class):
        name = build_device_name(domain, family, member)
        try:
            server = self[server_name]
        except KeyError:
            # Server does not exist, let's create it
            server = self.new_server(server_name)
        return server.new_device(
            name=name,
            domain=domain,
            family=family,
            member=member,
            device_class=device_class,
        )

    def del_device(self, domain, family, member):
        name = build_device_name(domain, family, member)
        for server in self.servers:
            if name in server:
                return server.del_device(domain=domain, family=family, member=member)
        raise ValueError(f"Device {name} does not exist")

    def write_json(self, json_file):
        with open(json_file, "w") as fp:
            json.dump(self.build_dict(), fp)

    def load(self, json_dict):
        for server in json_dict["servers"]:
            self.new_server(server["server_name"])
            for dev in server["devices"]:
                self.new_device(
                    server["server_name"],
                    dev["domain"],
                    dev["family"],
                    dev["member"],
                    dev["mascotclass"],
                )


class ServerRegistry(RegistryServicer):
    def __init__(self, save_file=None, load_file=None):
        RegistryServicer.__init__(self)
        self.data = {}
        self.save_file = save_file
        self.load_file = load_file
        self.registry = Registry()
        self.logger = logging.getLogger(ServerRegistry.__name__)
        if self.load_file:
            self.read_from_json_file()

    def read_from_json_file(self):
        try:
            with open(self.load_file, "r") as fp:
                data = json.load(fp)
        except FileNotFoundError:
            self.logger.error(f"Cannot load json file {self.load_file}")
        else:
            self.registry.load(data)

    def write_json_file(self):
        if self.save_file:
            self.registry.write_json(self.save_file)

    def CreateDevice(self, request, context):
        try:
            self.logger.info(f"Create Device Request: {format(request)}")
            devices = request.devices
            server_name = request.server_name.lower()
            for dev in devices:
                self.registry.new_device(
                    server_name,
                    dev.name.domain,
                    dev.name.family,
                    dev.name.identifier,
                    dev.mascotclass,
                )
            self.write_json_file()
            return ErrorReply(success=True)
        except Exception:
            self.logger.error(f"Create Device Failed: \n{traceback.format_exc()}")
            # Build an Error answer
            return ErrorReply(
                success=False, error_description=str(traceback.format_exc())
            )

    def DeleteDevice(self, request, context):
        try:
            self.logger.info(f"Delete Device Request: {request}")
            devices = request.devices
            for dev in devices:
                self.registry.del_device(dev.domain, dev.family, dev.identifier)
            self.write_json_file()
            return ErrorReply(success=True)
        except Exception:
            self.logger.error(f"Delete Device Failed: \n{traceback.format_exc()}")
            # Build an Error answer
            return ErrorReply(
                success=False, error_description=str(traceback.format_exc())
            )

    def PublishServer(self, request, context):
        self.logger.info(f"Publish Server Request: {request}")
        server_name = request.server_name.lower()
        host = request.host
        port = request.port
        try:
            server_dct = self.registry[server_name]
        except KeyError:
            err = f"Unknown server {server_name}"
            self.logger.error(f"Publish Server Failed: \n{traceback.format_exc()}")
            return ErrorReply(success=False, error_description=err)
        server_dct.port = port
        server_dct.host = host
        return ErrorReply(success=True)

    def UnpublishServer(self, request, context):
        self.logger.info(f"Unpublish Server Request: {request}")
        server_name = request.server_name.lower()
        try:
            server_dct = self.registry[server_name]
        except KeyError:
            err = f"Unknown server {server_name}"
            self.logger.error(f"Unpublish Server Failed: \n{traceback.format_exc()}")
            return ErrorReply(success=False, error_description=err)
        server_dct.port = 0
        server_dct.host = ""
        return ErrorReply(success=True)

    def ListServers(self, request, context):
        self.logger.info("List Servers Request")
        reply = ListServersReply()
        for server in self.registry:
            reply.server_name.append(server.server_name)
        reply.error.success = True
        return reply

    def ListDevices(self, request, context):
        self.logger.info("List Devices Request")
        server_name = request.server_name.lower()
        reply = ListDevicesReply()
        try:
            server = self.registry[server_name]
        except KeyError:
            reply.error.success = False
            reply.error.error_description = f"Unknown server {server_name}"
        else:
            for device in server.devices:
                devname = DeviceName(
                    domain=device.domain, family=device.family, identifier=device.member
                )
                device = Device(mascotclass=device.mascotclass, name=devname)
                reply.devices.extend([device])
            reply.error.success = True
        return reply

    def PublishDevice(self, request, context):
        self.logger.info(f"Publish Device Request: {request}")
        family = request.family
        domain = request.domain
        member = request.identifier
        name = build_device_name(domain, family, member)

        for server in self.registry:
            if name in server:
                server[name].exported = True
                return ErrorReply(success=True)
        err = f"Unknown device {name}"
        return ErrorReply(success=False, error_description=err)

    def UnpublishDevice(self, request, context):
        self.logger.info(f"Unpublish Device Request: {request}")
        family = request.family
        domain = request.domain
        member = request.identifier
        name = build_device_name(domain, family, member)

        for server in self.registry:
            if name in server:
                server[name].exported = False
                return ErrorReply(success=True)
        err = f"Unknown device {name}"
        return ErrorReply(success=False, error_description=err)

    def Locate(self, request, context):
        self.logger.info(f"Locate Request: {request}")
        name = "/".join([request.domain, request.family, request.identifier])
        reply = LocateReply()
        for server in self.registry:
            if name in server:
                reply.server_name = server.server_name
                reply.host = server.host
                reply.port = server.port
                reply.error.success = True
                return reply
        reply.error.success = False
        reply.error.error_description = f"Unknown device {name}"
        return reply


def run(port=None, host=None):
    FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT, level=logging.INFO)

    # Arguments parsing
    desc = "PyMascot Registry Server based on GRPC and JSON persistance."
    parser = argparse.ArgumentParser(prog="jsonregistry_server", description=desc)
    parser.add_argument(
        "--save-file",
        "-s",
        default="/tmp/database.json",
        action="store",
        dest="save_file",
        help="JSON file to save.",
    )
    parser.add_argument(
        "--load-file",
        "-l",
        default=None,
        action="store",
        dest="load_file",
        help="JSON file to load.",
    )

    arguments = parser.parse_args()

    # create a gRPC server
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    directory = ServerRegistry(arguments.save_file, arguments.load_file)
    add_RegistryServicer_to_server(directory, server)
    server.add_insecure_port("[::]:50000")
    server.start()
    try:
        while True:
            sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    run()
