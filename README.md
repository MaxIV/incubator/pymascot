# lib-maxiv-pymascot

<img align="center" src="https://nox.apps.okd.maxiv.lu.se/widget?conda_package=mascot&rpm_package=python36-mascot"/>

The mascot implementation in python


# Run the test

Run the test with tox
```bash
$ tox
```

Install tox
```bash
$ pip intsall tox
```

# About

Mascot is a [PyTango](https://github.com/tango-controls/pytango) like library based on [grpc](https://grpc.github.io/). It tries to provide the same API as the PyTango [High Level API](https://pytango.readthedocs.io/en/stable/server_api/server.html?highlight=level) on the server side. It also mimics the `tango.DeviceProxy` API and the `tango.Database` API. The goal is to provide user code compatibility (except for imports today) between PyTango and PyMascot


### Registry
A basic implement of the[mascot registry](https://gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry) based on json file is implemented. To start the database, run:

```bash
$ python -m mascot_registry.jsonregistry_server
```

Register one device:
```python
from mascot.database import Database
from mascot.dataclasses import DbDevInfo

db = Database("localhost:50000")
info = DbDevInfo(name="sys/mascot_test/1", server="MascotTest/1", klass="TestDevice")
db.add_device(info)
```

### Server implentaion example

```python

from time import time

from mascot.server import Device, attribute
from mascot.tango import AttrQuality


class TestDevice(Device):
    _ampli = 1234
    ampli = attribute(dtype=int)
    bool_attr = attribute(dtype=bool)
    float_attr = attribute(dtype=float)
    string_attr = attribute(dtype=str)
    error = attribute(dtype=bool, fget="raise_error")

    def read_ampli(self):
        return self._ampli

    def read_bool_attr(self):
        return True, AttrQuality.ATTR_INVALID

    def read_float_attr(self):
        return 34.5, 3550234237.123

    def read_string_attr(self):
        return ("Hello World", AttrQuality.ATTR_INVALID, time())

    def write_ampli(self, value):
        self._ampli = value
    def raise_error(self):
        raise ValueError("Some Error")


if __name__ == "__main__":
    TestDevice.run_server()
```

Run the server like:
```bash
$ TANGO_HOST=localhost:50000 python demo.py MascotTest/1
```

### Client example
```bash
$ TANGO_HOST=localhost:50000 ipython
```


```ipython
In [1]: from mascot.device_proxy import DeviceProxy

In [2]: ds = DeviceProxy("sys/mascot_test/1")

In [3]: ds.ampli
Out[3]: 1234

In [4]: ds.ampli = 14

In [5]: ds.read_attribute("ampli")
Out[5]: DeviceAttribute(data_format=<AttrDataFormat.SCALAR: 0>, dim_x=1, dim_y=0, has_failed=False, is_empty=False, name='ampli', quality=<AttrQuality.ATTR_VALID: 0>, value=14)

In [6]: ds.write_attribute("ampli", 1234)

In [7]: ds.get_attribute_list()
Out[7]: ['ampli', 'bool_attr', 'float_attr', 'string_attr', 'error', 'state', 'status']

In [8]: ds.error
---------------------------------------------------------------------------
DevFailed: Traceback (most recent call last):
  File "/home/antdup/projects/maxiv/lib-maxiv-pymascot/mascot/grpc_servicer.py", line 51, in ReadAttribute
    attr = device.read_attribute(attribute)
  File "/home/antdup/projects/maxiv/lib-maxiv-pymascot/mascot/mascot_server.py", line 81, in read_attribute
    values = attr_func()
  File "demo.py", line 30, in raise_error
    raise ValueError("Some Error")
ValueError: Some Error
```

# Proto file definition
The protofile definition can be found:
 - [Core](https://gitlab.maxiv.lu.se/kits-maxiv/lib-maxiv-mascot)
 - Registry(https://gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry)

# Status

## Attribute
Only Read and write scalar attribute is implemented now days (server and client).
Attribute configuration as a basic default implementation. It is not setable from the client side and there is no persistance


# PyTango test
PyTango client and server test has been imported to this project.
For each new feature, remove before starting developement, the pytest.xfail line in `tests/test_mascot/tests_from_tango/...`.
Be sure that PyTango test fails. Implement the new feature and then check that feature related PyTango test now succeed. This is the way that will garantie kind of user code compatibility between tango and mascot.
